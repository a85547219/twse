import talib
from commonCode import IndexEnum
from operator import itruediv
import numpy as np
from Service.databaseService import Database
from Tool.technicalTool import CrossOver, Trend


class TaIndex:
    """
        資料整理。
    """

    def get_index(self, stock_no):

        keys = [IndexEnum.DATE, IndexEnum.OPEN, IndexEnum.CLOSE, IndexEnum.HIGH, IndexEnum.LOW, IndexEnum.VOL,
                IndexEnum.VOL_OVERBOOKING, IndexEnum.AP, IndexEnum.MAP,
                IndexEnum.CI, IndexEnum.FIDL, IndexEnum.IT, IndexEnum.DL_SD, IndexEnum.DL_HG, IndexEnum.RT,
                IndexEnum.MA5, IndexEnum.DV5, IndexEnum.MA10, IndexEnum.DV10, IndexEnum.MA18, IndexEnum.DV18,
                IndexEnum.MA20, IndexEnum.DV20, IndexEnum.MA30, IndexEnum.DV30, IndexEnum.MA50, IndexEnum.DV50,
                IndexEnum.RSI5, IndexEnum.RSI10,
                IndexEnum.MIDDLEBAND, IndexEnum.UPPERBAND1, IndexEnum.LOWERBAND1, IndexEnum.UPPERBAND2,
                IndexEnum.LOWERBAND2, IndexEnum.BIAS,
                IndexEnum.K, IndexEnum.D, IndexEnum.PREDICT_CROSS_PRICE, IndexEnum.FAST_K, IndexEnum.FAST_D,
                IndexEnum.DIFF, IndexEnum.MACD, IndexEnum.HIST,
                IndexEnum.ADX, IndexEnum.PLUS_DI, IndexEnum.MINUS_DI]

        def _daily_doc(docs):
            return np.array([dict(zip(keys, doc)) for doc in zip(*docs)])

        def _map_doc(docs):
            return dict(zip(keys, docs))

        data = self._cal_index(stock_no)
        return _daily_doc(data), _map_doc(data)

    def _get_ii_by_name_and_reasign(self, stock_no, size):
        rst_list = []
        for data in Database.get_ii_by_name(stock_no):
            tmp = [0 for _ in range(size - len(data))]
            # tmp = np.array([0 for _ in range(size - len(data))])
            tmp.extend(data)
            rst_list.append(tmp)
        return rst_list

    def _cal_index(self, stock_no):
        date, open, close, high, low, vol = Database.get_doc_by_name(stock_no)
        ci, fi, it, dl_sd, dl_hg, rt = self._get_ii_by_name_and_reasign(stock_no, len(date))
        sma5, dv5 = self._ma(close, 5)
        sma10, dv10 = self._ma(close, 10)
        sma18, dv18 = self._ma(close, 18)
        sma20, dv20 = self._ma(close, 20)
        sma30, dv30 = self._ma(close, 30)
        sma50, dv50 = self._ma(close, 50)
        rsi5 = self._rsi(close, 5)
        rsi10 = self._rsi(close, 10)
        upperband1, middleband, lowerband1 = self._bband(close, nbdevup=1, nbdevdn=1)
        upperband2, middleband, lowerband2 = self._bband(close, nbdevup=2, nbdevdn=2)
        bias = self._bias(close, sma5)
        overbooking = self._vol_index(vol)
        fastk, fastd = self._fast_kd(high, low, close)
        k, d = self._kd(fastk, period=9)
        ap, m_ap = self._ap_index(close, high, low)
        kd_cross = self._reverse_kd(close, k, d, high, low, period=18)
        dif, macdsignal, macdhist = self._macd(close)
        adx, plus_di, minus_di = self._dmi(high, close, close)
        return date, open, close, high, low, vol, overbooking, ap, m_ap, \
               ci, fi, it, dl_sd, dl_hg, rt, \
               sma5, dv5, sma10, dv10, sma18, dv18, sma20, dv20, sma30, dv30, sma50, dv50, rsi5, rsi10, \
               middleband, upperband1, lowerband1, upperband2, lowerband2, \
               bias, k, d, kd_cross, fastk, fastd, dif, macdsignal, macdhist, adx, plus_di, minus_di

    def _vol_index(self, vol, period=5):
        pre_list = np.array([np.NaN for _ in range(0, period)])
        vol_overbooking = []
        for i in range(0, len(vol) - period):
            denominator = 1 if sum(vol[i:i + period]) == 0 else (sum(vol[i:i + period]) / period)
            vol_overbooking.append(vol[i + period] / denominator)
        return np.concatenate((pre_list, np.array(vol_overbooking)))

    def _ap_index(self, close, high, low):
        pre_list = np.array([np.NaN])
        amplitude = []
        max_amplitude = []
        close2 = close[1:]
        high = high[1:]
        low = low[1:]
        for i, j, k, l in zip(close, close2, high, low):
            amplitude.append((j - i) / i)
            max_amplitude.append((k - l) / i)
        return np.concatenate((pre_list, np.array(amplitude))), np.concatenate((pre_list, np.array(max_amplitude)))

    def _dmi(self, high, low, close):
        """
        :param high, low, close
        :return: ADX,  +DI, -DI
        """
        adx = talib.ADX(high, low, close, timeperiod=10)
        minus_di = talib.MINUS_DI(high, low, close, timeperiod=10)
        plus_di = talib.PLUS_DI(high, low, close, timeperiod=10)

        return adx, plus_di, minus_di

    def _ma(self, close, period=5):
        """
        :param close:
        :return: sma5, dv5, sma10, dv10, sma20, dv20, sma30, dv30, sma60, dv60
        """
        sma = talib.SMA(close, period)
        dv = np.append(np.array([np.NAN for _ in range(period)]), close[:-(period)])
        return sma, dv

    def _rsi(self, close, timeperiod):
        """
        :param close:
        :return:
        """
        return talib.RSI(close, timeperiod=timeperiod)

    def _slow_kd(self, high, low, close):
        """
        :param high,  low, close
        :return:  k ,d
        """
        return talib.STOCH(high, low, close, fastk_period=9, slowk_period=3, slowk_matype=0, slowd_period=3,
                           slowd_matype=0)

    def _fast_kd(self, high, low, close):
        """
        :param high, low, close
        :return:  fastK(RSV), fastD
        """
        return talib.STOCHF(high, low, close, fastk_period=9, fastd_period=3, fastd_matype=0)

    def _kd(self, fastK, period=8):
        pre_list = np.array([np.NaN for _ in range(0, period + 1)])
        rst_k, rst_d = [], []
        k, d = 50, 50
        for rsv in fastK[period + 1:]:
            k = (2 * k + rsv) / 3
            d = (2 * d + k) / 3
            rst_k.append(k)
            rst_d.append(d)
        return np.concatenate((pre_list, np.array(rst_k))), np.concatenate((pre_list, np.array(rst_d)))

    def _reverse_kd(self, c, k, d, h, l, period=8):
        # (收盤-最低)/(最高-最低)*100
        # 若隔日收盤為最高 RSV = 100
        # 若隔日收盤為最低 RSV = 0
        pre_list = np.array([np.NaN for _ in range(0, period + 1)])
        rst_p = []
        for i in range(0, len(c) - (period + 1)):
            b = i + period + 1
            fu_rsv = 3 * d[b] - 2 * k[b]
            max_c = max(h[i:b])
            min_c = min(l[i:b])
            rst_p.append(fu_rsv * (max_c - min_c) / 100 + min_c)
        return np.concatenate((pre_list, np.array(rst_p)))

    def _macd(self, close):
        """
        :param close:
        :return:  macd(diff), macdsignal(macd), macdhist
        """
        return talib.MACD(close, fastperiod=12, slowperiod=26, signalperiod=9)

    def _bias(self, close, ma):
        """
        BIAS=(close-(N-MA))/(MA5)*100%
        :param close:
        :param ma:
        :return:
        """
        return [np.NAN for _ in range(4)] + list(map(itruediv, close[4:] - ma[4:], ma[4:]))

    def _bband(self, close, timeperiod=20, nbdevup=2, nbdevdn=2, matype=0):
        """
        :param close:
        :return: upperband, middleband, lowerband
        """
        return talib.BBANDS(close, timeperiod=timeperiod, nbdevup=nbdevup, nbdevdn=nbdevdn, matype=matype)
