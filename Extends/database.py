from Instance.databaseInstance import Database
from Instance.decorator import Singleton
import pymongo


class BasicConnection(Singleton):

    def __init__(self):
        if not getattr(self, 'table_name', None):
            raise ValueError("%s must have a name" % type(self).__name__)

        self._db = Database().db

    def record(self, content):
        if type(content) is list:
            if len(content) == 1:
                self.record_one(content[0])
            else:
                self.record_many(content)
        else:
            self.record_one(content)

    @property
    def collection(self):
        return self._db[self.table_name]

    def remove_by_date(self, date):
        self._db[self.table_name].remove({'DATE': {'$gte': date}})

    def get_lasted_date(self):
        cursor = self._db[self.table_name].find({}, {'_id': 0, 'DATE': 1}).sort("DATE", pymongo.DESCENDING).limit(1)
        return cursor[0]['DATE'] if cursor.count() > 0 else None

    def count_by_date(self, date):
        return self._db[self.table_name].find({'DATE': {"$eq": date}}).count()
