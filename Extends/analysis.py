from abc import ABCMeta, abstractmethod
from TA.index import TaIndex
from Tool.technicalTool import CrossOver, Trend
from Tool.tradingTool import CommonTool
from commonCode import *
import numpy as np
from datetime import datetime
from ServiceImplement.DatabaseImpl.stockNoRepository import StockNoTable
from commonCode import *


class Trace(metaclass=ABCMeta):
    # 執行策略時，放入所需參數
    record = {RecordEnum.KD: []}
    # 提前"幾日"執行分析
    from_days = 1
    # 推薦原因
    _reason = []
    # 信件補充說明內容，通常為指數行情
    _note = []

    def __init__(self, _sn, _stop_loss=0.1):
        # 交易過程中出現最高價，日期
        self.current_max = 0
        self.current_date = None
        # analysis setting
        self.category = StockNoTable().get_category(_sn)
        self.stock_no = _sn
        self._stop_loss_point = _stop_loss
        # technical trace
        self.ma30_trend_func = Trend()
        self.ma18_cross_func = CrossOver()
        self.ma10_cross_func = CrossOver()
        self.kd_cross_func = CrossOver()
        self.bband_up_cross_func = CrossOver()
        self.dmi_cross_func = CrossOver()
        self.rsi_cross_func = CrossOver()
        self.macd_cross_func = CrossOver()
        # result
        self.fail_buy_date, self.fail_buy_price = [], []
        self.fail_sell_date, self.fail_sell_price = [], []
        self.stop_loss_date, self.stop_loss_price = [], []
        self.sell_win_date, self.sell_win_price = [], []
        self.sell_loss_date, self.sell_loss_price = [], []
        self.best_sell_price = []
        self.buy_date_all, self.buy_price_all = [], []
        self.one_trade_return, self.one_trade_cost, self.one_trade_keep_days = [], [], []
        self.buy_price, self.buy_date = [], []
        # basic data
        self.daily_doc, self.map_doc = None, None
        # order data
        self.action_type, self.order_price = TradeAction.DO_NOTHING, 0
        # fig data
        self.win_date_collection, self.win_price_collection = [], []
        self.loss_date_collection, self.loss_price_collection = [], []
        self.keep_date_collection, self.keep_price_collection = [], []
        # detail data
        self.win_reason_collection, self.loss_reason_collection, self.keep_reason_collection = [], [], []

    def after_success_sell(self, price, index_doc):
        pass

    def after_success_buy(self, price, index_doc):
        pass

    def record_function(self, doc):
        pass

    @abstractmethod
    def buy_or_sell(self, index_doc):
        """
        :param index_doc:
        :return: TradeAction, price
        """
        pass

    @staticmethod
    def comment():
        """
        :return: list
        """
        return []

    def trade_reason(self):
        return self._reason, self._note

    def run_analysis(self):
        # start = datetime.now().timestamp()
        self.daily_doc, self.map_doc = TaIndex().get_index(self.stock_no)

        # end = datetime.now().timestamp()
        # print('index: {}'.format(end - start))

        def _cal_technical_point(_doc):
            # _doc[IndexEnum.TREND] = self.ma30_trend_func.check(_doc[IndexEnum.MA30])
            # _doc[IndexEnum.MA18_CROSS], _doc[IndexEnum.MA18_POINT], _doc[
            #     IndexEnum.MA18_TREND] = self.ma18_cross_func.check(
            #     _doc[IndexEnum.MA18], _doc[IndexEnum.MA50])
            # _doc[IndexEnum.MA10_CROSS], _doc[IndexEnum.MA10_POINT], _doc[
            #     IndexEnum.MA10_TREND] = self.ma10_cross_func.check(
            #     _doc[IndexEnum.MA10], _doc[IndexEnum.MA50])
            _doc[TechEnum.KD_CROSS], _doc[TechEnum.KD_POINT], _doc[TechEnum.KD_TREND] = self.kd_cross_func.check(
                _doc[IndexEnum.K],
                _doc[IndexEnum.D])
            # _doc[IndexEnum.BBAND_CROSS], _doc[IndexEnum.BBAND_POINT], _doc[
            #     IndexEnum.BBAND_TREND] = self.bband_up_cross_func.check(
            #     _doc[IndexEnum.CLOSE],
            #     _doc[IndexEnum.UPPERBAND1])
            _doc[TechEnum.DMI_CROSS], _doc[TechEnum.DMI_POINT], _doc[TechEnum.DMI_TREND] = self.dmi_cross_func.check(
                _doc[IndexEnum.PLUS_DI],
                _doc[IndexEnum.MINUS_DI])
            _doc[TechEnum.MACD_CROSS], _doc[TechEnum.MACD_POINT], _doc[
                TechEnum.MACD_TREND] = self.macd_cross_func.check(
                _doc[IndexEnum.DIFF],
                _doc[IndexEnum.MACD])
            _doc[TechEnum.RSI_CROSS], _doc[TechEnum.RSI_POINT], _doc[TechEnum.RSI_TREND] = self.rsi_cross_func.check(
                _doc[IndexEnum.RSI5],
                _doc[IndexEnum.RSI10])

        def _check_order_deal_or_stop_loss(current_index):
            """
                event flow: stop, sell, buy (only one action)
            :param current_index:
            """

            def _do_sell(date, price):
                def _record():
                    cost = sum(self.buy_price) / len(self.buy_price)
                    rst = (price - cost) / cost
                    diff_day = (date - self.buy_date[0]).days

                    if rst > 0:
                        self.sell_win_date.append(date)
                        self.sell_win_price.append(price)
                    else:
                        self.sell_loss_date.append(date)
                        self.sell_loss_price.append(price)
                    self.best_sell_price.append((self.current_date, self.current_max))
                    self.one_trade_cost.append(cost)
                    self.one_trade_keep_days.append(diff_day)
                    self.one_trade_return.append(rst)
                    self.current_max, self.current_date = 0, None

                def _clean():
                    self.buy_price = []
                    self.buy_date = []

                if self.buy_price:
                    _record()
                    _clean()

            def _do_buy(date, price):
                self.buy_price.append(price)
                self.buy_date.append(date)
                self.buy_date_all.append(date)
                self.buy_price_all.append(price)
                buy_max = max(self.buy_price)
                if buy_max > self.current_max or self.current_date < self.buy_date[0]:
                    self.current_max = buy_max
                    self.current_date = self.buy_date[self.buy_price.index(self.current_max)]

            def _fail_sell(date, price):
                self.fail_sell_date.append(date)
                self.fail_sell_price.append(price)

            def _fail_buy(date, price):
                self.fail_buy_date.append(date)
                self.fail_buy_price.append(price)

            if self.current_max < current_index[IndexEnum.HIGH]:
                self.current_max, self.current_date = current_index[IndexEnum.HIGH], current_index[IndexEnum.DATE]

            if self.action_type is TradeAction.DO_STOP:
                self.stop_loss_price.append(current_index[IndexEnum.OPEN])
                _do_sell(current_index[IndexEnum.DATE], current_index[IndexEnum.OPEN])
                self.stop_loss_date.append(current_index[IndexEnum.DATE])
                self.after_success_sell(current_index[IndexEnum.OPEN], current_index)
            elif self.action_type is TradeAction.SELL:
                if self.order_price > current_index[IndexEnum.HIGH]:
                    _fail_sell(current_index[IndexEnum.DATE], self.order_price)
                else:
                    if self.order_price >= current_index[IndexEnum.LOW]:
                        sale_price = self.order_price
                    else:
                        sale_price = current_index[IndexEnum.OPEN]
                    _do_sell(current_index[IndexEnum.DATE], sale_price)
                    self.after_success_sell(sale_price, current_index)
            elif self.action_type is TradeAction.MUST_SELL:
                if current_index[IndexEnum.HIGH] >= self.order_price >= current_index[IndexEnum.LOW]:
                    sale_price = self.order_price
                else:
                    sale_price = current_index[IndexEnum.CLOSE]
                _do_sell(current_index[IndexEnum.DATE], sale_price)
                self.after_success_sell(sale_price, current_index)
            elif self.action_type is TradeAction.BUY:
                if self.order_price < current_index[IndexEnum.LOW]:
                    _fail_buy(current_index[IndexEnum.DATE], self.order_price)
                else:
                    if current_index[IndexEnum.HIGH] >= self.order_price:
                        buy_price = self.order_price
                    else:
                        buy_price = current_index[IndexEnum.OPEN]
                    _do_buy(current_index[IndexEnum.DATE], buy_price)
                    self.after_success_buy(buy_price, current_index)
            elif self.action_type is TradeAction.MUST_BUY:
                if current_index[IndexEnum.HIGH] >= self.order_price >= current_index[IndexEnum.LOW]:
                    buy_price = self.order_price
                else:
                    buy_price = current_index[IndexEnum.CLOSE]
                _do_buy(current_index[IndexEnum.DATE], buy_price)
                self.after_success_buy(buy_price, current_index)
            else:
                pass

        def _analyze_index_and_gen_order(current_doc):
            self.action_type, self.order_price = None, None
            order_result = self.buy_or_sell(current_doc)
            if order_result:
                self.action_type, self.order_price = order_result
                self.order_price = CommonTool.price_rule(self.order_price)
            self.record_function(current_doc)

        def _check_stop_loss(current_doc):
            if self.buy_price:
                average_cost = round(sum(self.buy_price) / len(self.buy_price), 3)
                if (current_doc[IndexEnum.CLOSE] - average_cost) < -(self._stop_loss_point * average_cost):
                    self.action_type = TradeAction.DO_STOP
                    self.order_price = None

        def _after_job():
            def record_win():
                self.win_reason_collection.append(
                    (self.keep_reason_collection,
                     (self.daily_doc[np.where(date_list == win_date)[0][0] - 1], self.sell_win_price[sell_win_index]),
                     self.best_sell_price[sell_win_index + sell_loss_index]))
                self.win_date_collection += self.keep_date_collection
                self.win_price_collection += self.keep_price_collection
                self.keep_reason_collection, self.keep_date_collection, self.keep_price_collection = [], [], []

            def record_loss():
                self.loss_date_collection += self.keep_date_collection
                self.loss_price_collection += self.keep_price_collection
                self.loss_reason_collection.append(
                    (self.keep_reason_collection,
                     (self.daily_doc[np.where(date_list == loss_date)[0][0] - 1],
                      self.sell_loss_price[sell_loss_index]), self.best_sell_price[sell_win_index + sell_loss_index]))
                self.keep_reason_collection, self.keep_date_collection, self.keep_price_collection = [], [], []

            # 整理圖示，細節的資料
            date_list = self.map_doc[IndexEnum.DATE]
            sell_win_index, sell_loss_index = 0, 0
            win_date = self.sell_win_date[sell_win_index] if self.sell_win_date else datetime(9999, 12, 31)
            loss_date = self.sell_loss_date[sell_loss_index] if self.sell_loss_date else datetime(9999, 12, 31)
            for i, j in zip(self.buy_date_all, self.buy_price_all):
                buy_date_index = np.where(date_list == i)[0][0]
                if win_date <= i:
                    record_win()
                    sell_win_index += 1
                    win_date = self.sell_win_date[sell_win_index] if sell_win_index < len(
                        self.sell_win_date) else datetime(9999, 12, 31)
                elif loss_date <= i:
                    record_loss()
                    sell_loss_index += 1
                    loss_date = self.sell_loss_date[sell_loss_index] if sell_loss_index < len(
                        self.sell_loss_date) else datetime(9999, 12, 31)
                self.keep_date_collection.append(i)
                self.keep_price_collection.append(j)
                self.keep_reason_collection.append((self.daily_doc[buy_date_index - 1], j))

            if win_date != datetime(9999, 12, 31):
                record_win()
            elif loss_date != datetime(9999, 12, 31):
                record_loss()
            self.keep_reason_collection.append((self.current_date, self.current_max))

        # start = datetime.now().timestamp()
        start_day = 0 if self.from_days == 0 else len(self.daily_doc) - self.from_days
        for _doc in self.daily_doc[start_day:]:
            # 盤中，依照評估的買賣點，嘗試交易。
            _check_order_deal_or_stop_loss(_doc)
            # 盤後，指標型態追蹤
            _cal_technical_point(_doc)
            # 盤後，檢查指標，評估新的買賣點。
            _analyze_index_and_gen_order(_doc)
            # 檢查成本是否已達停損點。
            _check_stop_loss(_doc)
        # end = datetime.now().timestamp()
        # print('in: {}'.format(end - start))
        # start = datetime.now().timestamp()
        _after_job()
        # end = datetime.now().timestamp()
        # print('after: {}'.format(end - start))

    def get_figure_data(self):
        return self.sell_win_date, self.sell_win_price, self.sell_loss_date, self.sell_loss_price, self.stop_loss_date, self.stop_loss_price, self.fail_buy_date, self.fail_buy_price, self.fail_sell_date, self.fail_sell_price, self.win_date_collection, self.win_price_collection, self.loss_date_collection, self.loss_price_collection, self.keep_date_collection, self.keep_price_collection

    def get_detail_data(self):
        return self.win_reason_collection, self.loss_reason_collection, self.keep_reason_collection

    def get_order_data(self):
        return self.stock_no, self.action_type, self.order_price, self.trade_reason()

    def get_basic_data(self):
        return self.daily_doc, self.map_doc

    def get_other_data(self):
        return self.buy_date_all

    def get_report_data(self):
        win_times = len(self.sell_win_date)
        total_return = sum(self.one_trade_return)
        total_days = sum(self.one_trade_keep_days)
        max_return = max(self.one_trade_return, default=0)
        min_return = min(self.one_trade_return, default=0)
        max_keep_days = max(self.one_trade_keep_days, default=0)
        min_keep_days = min(self.one_trade_keep_days, default=0)

        return {
            ReportEnum.STOCK_NO: self.stock_no,
            ReportEnum.WIN_RATE: round(win_times / len(self.one_trade_return) * 100, 3) if len(
                self.one_trade_return) else 0,
            ReportEnum.RETURN_AVG: round(total_return / len(self.one_trade_return) * 100, 3) if len(
                self.one_trade_return) else 0,
            ReportEnum.RETURN_ACC: round(total_return * 100, 3),
            ReportEnum.RETURN_MAX: round(max_return * 100, 3),
            ReportEnum.RETURN_MIN: round(min_return * 100, 3),
            ReportEnum.TRADE_TIMES: len(self.one_trade_return), ReportEnum.BUY_TIMES: len(self.buy_price_all),
            ReportEnum.WIN_TIMES: len(self.sell_win_date), ReportEnum.LOSE_TIMES: len(self.sell_loss_date),
            ReportEnum.BUY_FAIL: len(self.fail_buy_price), ReportEnum.SELL_FAIL: len(self.fail_sell_price),
            ReportEnum.STOP: len(self.stop_loss_date),
            ReportEnum.KEEP_DAY_AVG: round(total_days / len(self.buy_price_all), 3) if len(self.buy_price_all) else 0,
            ReportEnum.KEEP_DAY_LONG: max_keep_days, ReportEnum.KEEP_DAY_SHORT: min_keep_days}
