from Instance.databaseInstance import Database
from abc import ABCMeta, abstractmethod
import errno
from socket import error as socket_error
from datetime import datetime
import requests
import settings
import time
import logging
import sys
import traceback


class Spider(metaclass=ABCMeta):

    def __init__(self, date=datetime.now()):
        self.date = date.replace(hour=0, minute=0, second=0, microsecond=0)
        self.date_str = self.date.strftime("%Y%m%d")
        self._db = Database().db
        self._init_setting(self.url, self.table_name)
        self.common_setting()
        self.params = self.params

    def get_collection(self, table_name=None):
        if not table_name:
            table_name = self.table_name
        return self._db[table_name]

    def _init_setting(self, url, table_name):
        """init url, param, table_name"""
        if url is not None:
            self.url = url
        elif not getattr(self, 'url', None):
            raise ValueError("%s must have a url" % type(self).__name__)
        if table_name is not None:
            self.table_name = table_name
        elif not getattr(self, 'table_name', None):
            raise ValueError("%s must have a table_name" % type(self).__name__)

    @abstractmethod
    def extract(self, content):
        """define how to get context"""
        pass

    def common_setting(self):
        """default common setting"""
        self.params['response'] = 'json'
        self.params['date'] = self.date_str
        self.params['connection'] = 'close',
        self.params['_'] = str(round(time.time() * 1000) - 500)

    def query(self):
        if isinstance(self.url, list):
            pages = []
            for url in self.url:
                pages.append(requests.get(url=url, params=self.params))
            return pages
        else:
            return requests.get(url=self.url, params=self.params)

    def check_status(self, page):
        """check connection status and log it"""
        content = page.json()
        if content['stat'] == 'OK':
            return content
        else:
            if self.date.weekday() != 5:
                logging.getLogger('WARNING').warning(
                    'Date: {}. Job: {}. empty '.format(self.date_str, type(self).__name__))
            return None

    def save(self, content):
        self.get_collection().insert_many(content)

    def _error_format(self, content):
        return 'Date: {}. Job: {}. {} '.format(self.date_str, type(self).__name__, content)

    def run_spider(self):
        message = ""

        def do():
            nonlocal message
            logger = logging.getLogger('PROGRESS')
            start = time.time()
            self.common_setting()
            # 抓取資料
            page = self.query()
            content = self.check_status(page)
            if content is None:
                return
            # 取出資料
            db_data = self.extract(content)
            # 儲存資料
            self.save(db_data)
            end = time.time()
            message = self._error_format('Complete. run Time: {}'.format(round(end - start, 2)))
            logger.info(message)

        try:
            time.sleep(settings.QUERY_DELAY_TIME)
            do()
        except TypeError:
            if self.date.weekday() != 5:
                message = self._error_format('empty')
                logging.getLogger('WARNING').warning(message)
        except socket_error as serr:
            if serr.errno != errno.ECONNREFUSED:
                raise serr
            message = self._error_format(self._log_detail(serr))
            logging.getLogger('ERROR').error(message)
            time.sleep(1800)
        except Exception as e:
            message = self._error_format(self._log_detail(e))
            logging.getLogger('ERROR').error(message)
        return message

    def _log_detail(self, e):
        error_class = e.__class__.__name__  # 取得錯誤類型
        detail = e.args[0]  # 取得詳細內容
        cl, exc, tb = sys.exc_info()  # 取得Call Stack
        last_call_stack = traceback.extract_tb(tb)[-1]  # 取得Call Stack的最後一筆資料
        file_name = last_call_stack[0]  # 取得發生的檔案名稱
        line_num = last_call_stack[1]  # 取得發生的行號
        func_name = last_call_stack[2]  # 取得發生的函數名稱
        return "File \"{}\", line {}, in {}: [{}] {}".format(file_name, line_num, func_name, error_class, detail)
