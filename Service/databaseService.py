from ServiceImplement.DatabaseImpl.miIndexRepository import *
from ServiceImplement.DatabaseImpl.bwibbuRepository import *
from ServiceImplement.DatabaseImpl.cyqRepository import *
from ServiceImplement.DatabaseImpl.iiRepository import *
from ServiceImplement.DatabaseImpl.stockNoRepository import *
from ServiceImplement.DatabaseImpl.twtbRepository import *
from ServiceImplement.DatabaseImpl.emailRecordRepo import *
from ServiceImplement.DatabaseImpl.tpexRepository import *


class Database:

    @staticmethod
    def get_doc_by_name(stock_no):
        return MiIndexRepo().get_doc_by_name(stock_no)

    @staticmethod
    def get_ii_by_name(stock_no):
        return IiRepo().get_doc_by_name(stock_no)

    @staticmethod
    def get_lasted_date_based_on_miindex():
        return MiIndexRepo().get_lasted_date()

    @staticmethod
    def get_summary_by_date(date):
        return IiSumRepo().get_sum_by_date(date)

    @staticmethod
    def get_all_stocks():
        rst = MiIndexRepo().get_all_sn()
        # 富邦VIX
        # rst.append("00677U")
        # 元大台灣反1
        # rst.append("00632R")
        # 元大S&P原油正2
        # rst.append("00672L")
        return rst

    @staticmethod
    def _get_all_collection():
        return {'ii': IiRepo(),
                'ii_sum': IiSumRepo(),
                'mi': MiRepo(),
                'mi_index': MiIndexRepo(),
                'mi_index_ix': MiIndexIxRepo(),
                'mi_index_st': MiIndexStRepo(),
                'mi_index_sum': MiIndexSumRepo(),
                'mi_index_tri': MiIndexTriRepo(),
                'tpex_3it': Tpex3ItRepo(),
                'tpex_index': TpexIndex(),
                'twtb4u': Twtb4uRepo(),
                'twtb4u_ms': Twtb4uMsRepo(),
                'bwibbu': BwibbuRepo(),
                'cyq': CyqRepo()}

    @staticmethod
    def daily_delete(date):
        for col in Database._get_all_collection().values():
            col.remove_by_date(date)

    @staticmethod
    def get_lasted_date_for_all():
        date_dict = {}
        for key, value in Database._get_all_collection().items():
            lasted_date = value.get_lasted_date()
            date_dict[key] = lasted_date

        return date_dict

    @staticmethod
    def check_latest_count_by_date(date):
        date_dict = {}
        for key, value in Database._get_all_collection().items():
            lasted_date = value.get_lasted_date()
            v = date_dict.get(lasted_date.strftime('%Y-%m-%d'), "")
            v += key + "。 "
            date_dict[lasted_date.strftime('%Y-%m-%d')] = v
        for key, value in date_dict.items():
            print("{}: {}".format(key, value))

        print(date)
        for key, value in Database._get_all_collection().items():
            count = value.count_by_date(date)
            print("{}: {}".format(key, count))
