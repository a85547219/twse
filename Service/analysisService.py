from Tool.matplot import Fig
from commonCode import *
from Tool.reportTool import PrintReport as pr
import multiprocessing as mp
from joblib import Parallel, delayed
from ServiceImplement.AnalysisImpl.kdStrategy import KdStrategy
from ServiceImplement.AnalysisImpl.coinStrategy import CoinStrategy
from datetime import datetime
from Service.databaseService import Database


class Analysis:

    @staticmethod
    def show_figure(dict_data, figure_setting_dict):
        daily_doc, map_doc = dict_data[AnalysisResultEnum.BASIC]
        sell_win_date, sell_win_price, sell_loss_date, sell_loss_price, stop_loss_date, stop_loss_price, fail_buy_date, fail_buy_price, fail_sell_date, fail_sell_price, win_date_collection, win_price_collection, loss_date_collection, loss_price_collection, keep_date_collection, keep_price_collection = \
            dict_data[AnalysisResultEnum.FIGURE]
        fig = Fig(daily_doc, map_doc)
        fig.set_title_name(dict_data[AnalysisResultEnum.ORDER][0])
        fig.set_fig_config(tag=figure_setting_dict.get('line', False),
                           win=figure_setting_dict.get('winLine', False),
                           loss=figure_setting_dict.get('lossLine', False),
                           fail=figure_setting_dict.get('failLine', False))
        fig.run((win_date_collection, win_price_collection),
                (sell_win_date, sell_win_price),
                (loss_date_collection, loss_price_collection),
                (sell_loss_date, sell_loss_price),
                (keep_date_collection, keep_price_collection),
                (stop_loss_date, stop_loss_price),
                (fail_buy_date, fail_buy_price),
                (fail_sell_date, fail_sell_price))

    @staticmethod
    def after_run_job(dict_data, show_setting_dict):
        _report_col, _date_set = [], set()
        for doc in dict_data:
            _report_col.append(doc[AnalysisResultEnum.REPORT])
            _date_set.update(doc[AnalysisResultEnum.OTHER])
            if show_setting_dict.get('showDetail', False):
                win_reason, loss_reason, keep_reason = doc[AnalysisResultEnum.DETAIL]
                pr.print_detail(win_reason, loss_reason, keep_reason)
            if show_setting_dict.get('showReport', False):
                pr.print_report(doc[AnalysisResultEnum.REPORT])
                pr.print_order(doc[AnalysisResultEnum.ORDER])
        if show_setting_dict.get('showSummary', False):
            pr.print_summary(_report_col, _date_set)

    @staticmethod
    def do(stocks, mode, stop_loss_point=1, cpu_count=mp.cpu_count(), strategy=KdStrategy):
        def select_job_container():
            if mode is RunModeEnum.ONE:
                return Analysis._all_container
            elif mode is RunModeEnum.MANY:
                return Analysis._analysis_container
            else:
                return Analysis._mail_container

        strategies = [strategy(x, stop_loss_point) for x in stocks]
        job_container = select_job_container()
        rst = Parallel(n_jobs=cpu_count)(delayed(job_container)(run) for run in strategies)
        return rst

    @staticmethod
    def _mail_container(obj):
        obj.run_analysis()
        return {AnalysisResultEnum.ORDER: obj.get_order_data()}

    @staticmethod
    def _analysis_container(obj):
        obj.run_analysis()
        return {
            AnalysisResultEnum.DETAIL: obj.get_detail_data(),
            AnalysisResultEnum.ORDER: obj.get_order_data(),
            AnalysisResultEnum.REPORT: obj.get_report_data(),
            AnalysisResultEnum.OTHER: obj.get_other_data()
        }

    @staticmethod
    def _all_container(obj):
        # 畫圖用
        obj.from_days = 0
        obj.run_analysis()
        return {AnalysisResultEnum.BASIC: obj.get_basic_data(),
                AnalysisResultEnum.ORDER: obj.get_order_data(),
                AnalysisResultEnum.FIGURE: obj.get_figure_data(),
                AnalysisResultEnum.DETAIL: obj.get_detail_data(),
                AnalysisResultEnum.REPORT: obj.get_report_data()}

    @staticmethod
    def mail(stock_list, strategy=CoinStrategy):
        rst = Analysis.do(stock_list, RunModeEnum.MAIL, strategy=strategy)
        now_time = datetime.now().replace(hour=0, minute=0, second=0, microsecond=0)
        title = "Report " + now_time.strftime("%Y/%m/%d")
        # 大盤資料
        summary = pr.summary_to_html(Database.get_summary_by_date(now_time))
        # 個股分析
        daily = pr.daily_data_to_html([x[AnalysisResultEnum.ORDER] for x in rst])
        # 策略註解
        strategy_comment = pr.strategy_comment_to_html(strategy.comment())
        # 共用註解
        common_comment = pr.comment_to_html(Database.get_lasted_date_based_on_miindex(), len(stock_list))
        # 寄出信件。
        pr.send_mail(title, summary, daily, common_comment, strategy_comment)

    @staticmethod
    def console(stock_list, strategy=CoinStrategy):
        rst = Analysis.do(stock_list, RunModeEnum.MAIL, strategy=strategy)
        pr.daily_data_to_print([x[AnalysisResultEnum.ORDER] for x in rst])
