import os
from Tool.loggingTool import LogTool
from datetime import timedelta
from ServiceImplement.ParserImpl.tpex import OTC, ItTrade
from ServiceImplement.ParserImpl.exchange import MI, MI_INDEX, BWIBBU, TWTB4U
from ServiceImplement.ParserImpl.fund import T86, BFI82U
from ServiceImplement.ParserImpl.financial import Mon, QUA
from ServiceImplement.ParserImpl.tdcc import CYQ
from Service.databaseService import Database


class Parser:

    def __init__(self):
        self._logging_init()

    def _logging_init(self):
        if not os.path.isdir('logs'):
            os.makedirs('logs')

        LogTool.setLogger('PROGRESS', 'progress')
        LogTool.setLogger('WARNING', 'warning')
        LogTool.setLogger('ERROR', 'error')

    def daily_parser(self, from_date, to_date=None):
        def scrapy_target():
            if job_date['mi'] < from_date:
                MI(from_date).run_spider()

            if job_date['mi_index'] < from_date:
                MI_INDEX(from_date).run_spider()
            # II
            if job_date['ii'] < from_date:
                T86(from_date).run_spider()
            if job_date['ii_sum'] < from_date:
                BFI82U(from_date).run_spider()
            # one day trading
            if job_date['twtb4u'] < from_date:
                TWTB4U(from_date).run_spider()
            #
            if job_date['bwibbu'] < from_date:
                BWIBBU(from_date).run_spider()
            # TPEX
            if job_date['tpex_3it'] < from_date:
                ItTrade(from_date).run_spider()
            if job_date['tpex_index'] < from_date:
                OTC(from_date).run_spider()

        job_date = Database.get_lasted_date_for_all()
        to_date = from_date if to_date is None else to_date
        while from_date <= to_date:
            if from_date.weekday() in [5, 6]:
                from_date += timedelta(1)
                continue
            scrapy_target()
            from_date += timedelta(1)
        print('job down')

    def week_parser(self):
        CYQ().run_spider()

    def fin_mon_parser(self, date):
        if date.month == 1:
            real_year, real_month = date.year - 1, 12
        else:
            real_year, real_month = date.year, date.month
        Mon(real_year, real_month - 1).run_spider()

    def fin_quo_parser(self, date):
        def season():
            if date.month <= 4:
                return 4
            elif date.month <= 6:
                return 1
            elif date.month <= 9:
                return 2
            else:
                return 3

        QUA(date.year, season()).run_spider()

    def auto_script(self, is_basic, from_date, to_date=None):
        def basic():
            if job_date['mi'] < from_date:
                MI(from_date).run_spider()
            if job_date['mi_index'] < from_date:
                MI_INDEX(from_date).run_spider()
            if job_date['tpex_3it'] < from_date:
                OTC(from_date).run_spider()
            if job_date['tpex_index'] < from_date:
                ItTrade(from_date).run_spider()
            if job_date['ii'] < from_date:
                T86(from_date).run_spider()
            if job_date['ii_sum'] < from_date:
                BFI82U(from_date).run_spider()

        def other():
            # one day trading
            if job_date['twtb4u'] < from_date:
                TWTB4U(from_date).run_spider()
            #
            if job_date['bwibbu'] < from_date:
                BWIBBU(from_date).run_spider()

        job_date = Database.get_lasted_date_for_all()
        to_date = from_date if to_date is None else to_date
        while from_date <= to_date:
            if from_date.weekday() in [5, 6]:
                from_date += timedelta(1)
                continue
            if is_basic:
                basic()
            else:
                other()
            from_date += timedelta(1)
        print('job down')
