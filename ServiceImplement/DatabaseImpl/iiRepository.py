from Extends.database import BasicConnection
import numpy as np
import pymongo
from pymongo import UpdateOne


class IiRepo(BasicConnection):
    table_name = 'ii'

    def get_doc_by_name(self, stock_no):
        """
        get Date, CI_B&S(外陸資), FI&DL_B&S(外資自營商), IT_B&S(投信), DL_B&S(SD)(自營商-自買), DL_B&S(HG)(自營商-避險), RT(MAX(法人買，法人賣))
        :param stock_no:
        :return: date[] CI[] ....
        """
        ci, fi, it, dl_sd, dl_hg, rt = [], [], [], [], [], []
        for doc in self.collection.find({'SN': stock_no},
                                        {'_id': 0, 'CI': 1, 'FIDL': 1, 'IT': 1, 'DL_SD': 1,
                                         'DL_HG': 1, 'RT': 1}).sort('DATE', pymongo.ASCENDING):
            ci.append(doc.get('CI'))
            fi.append(doc.get('FIDL'))
            it.append(doc.get('IT'))
            dl_sd.append(doc.get('DL_SD'))
            dl_hg.append(doc.get('DL_HG'))
            rt.append(doc.get('RT'))
        return ci, fi, it, dl_sd, dl_hg, rt

    def get_all_sn(self):
        tmp = []
        for data in self.collection.find({}, {'_id': 0, 'SN': 1}).distinct('SN'):
            tmp.append(data)
        return tmp

    def fix(self):

        all_sn = []
        for data in self.collection.find({}, {'_id': 0, 'SN': 1}).distinct('SN'):
            all_sn.append(data)
        for sn in all_sn:
            modified_list = []
            for doc in self.collection.find({'SN': sn}):
                buy = doc['CI_B'] + doc['FIDL_B'] + doc['IT_B'] + doc['DL_B(SD)'] + doc['DL_B(HG)']
                sell = doc['CI_S'] + doc['FIDL_S'] + doc['IT_S'] + doc['DL_S(SD)'] + doc['DL_S(HG)']
                doc['RT'] = max(buy, sell)
                modified_list.append(doc)
            update_list = []
            for row in modified_list:
                update_list.append(UpdateOne({'_id': row['_id']}, {'$set': row}, upsert=True))
            if update_list:
                self.collection.bulk_write(update_list)


class IiSumRepo(BasicConnection):
    table_name = 'ii_sum'

    def get_sum_by_date(self, date):
        rst = []
        for data in self.collection.find({'DATE': date}, {'_id': 0, 'MCM': 1, 'B_A': 1, 'S_A': 1, 'B&S_A': 1}):
            rst.append(data)
        return rst
