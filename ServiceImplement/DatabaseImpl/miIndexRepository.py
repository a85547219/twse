from Extends.database import BasicConnection
import numpy as np
import pymongo


class MiRepo(BasicConnection):
    table_name = 'mi'


class MiIndexRepo(BasicConnection):
    table_name = 'mi_index'

    def find_by_date(self, date):
        return [doc for doc in self.collection.find({'DATE': date}, {'_id': 0, 'SN': 1, 'C': 1, 'V': 1, 'MAP': 1}) if
                doc.get('MAP', 0) > 0.05]

    def get_all_sn(self):
        tmp = []
        date = self.get_lasted_date()
        for data in self.collection.find({'DATE': date}, {'_id': 0, 'SN': 1}).distinct('SN'):
            if len(data) == 4:
                tmp.append(data)
        tmp.sort()
        return tmp

    def get_doc_by_name(self, stock_no):
        """
        get Date, Open, Close, High, Low
        :param stock_no
        :return: date[]  open[] close[] high[] low[] vol[]
        """
        date, op, close, high, low, vol = [], [], [], [], [], []
        for doc in self.collection.find({'SN': stock_no, 'ACT': True},
                                        {'_id': 0, 'DATE': 1, 'C': 1, 'O': 1, 'H': 1, 'L': 1, 'V': 1}).sort(
            'DATE', pymongo.ASCENDING):
            date.append(doc['DATE'])
            close.append(doc['C'])
            op.append(doc['O'])
            high.append(doc['H'])
            low.append(doc['L'])
            vol.append(doc['V'])

        return np.array(date), np.array(op), np.array(close), np.array(high), np.array(low), np.array(vol)

    def get_month_by_sn(self, sn):
        rst_list = []

        collection = self.collection.find({'SN': sn, 'ACT': True},
                                          {'_id': 0, 'DATE': 1, 'C': 1, 'O': 1, 'H': 1, 'L': 1, 'V': 1}).sort(
            'DATE', pymongo.ASCENDING)
        year, month = collection[0]['DATE'].year, collection[0]['DATE'].month
        last_doc = collection[collection.count() - 1]
        this_month = False
        O, C, H, L, AT, AV = collection[0]['O'], 0, 0, 9999, 0, 0
        for doc in collection:
            if month != doc['DATE'].month:
                rst_list.append(
                    {'Y': year, 'M': month, 'C': C, 'O': O, 'H': H, 'L': L, 'AT': round(AT / AV, 2),
                     'AV': round(AV / 1000, 1)})
                year, month = doc['DATE'].year, doc['DATE'].month
                O, C, H, L, AT, AV = doc['O'], doc['C'], doc['H'], doc['L'], doc['V'] * doc['C'], doc['V']
                this_month = False
            else:
                C, H, L = doc['C'], max(doc['H'], H), min(doc['L'], L)
                AT += doc['V'] * doc['C']
                AV += doc['V']
                this_month = True
        if this_month:
            rst_list.append(
                {'Y': last_doc['DATE'].year, 'M': last_doc['DATE'].month, 'C': C, 'O': O,
                 'H': H, 'L': L, 'AT': round(AT / AV, 2),
                 'AV': round(AV / 1000, 1)})
        return rst_list


class MiIndexIxRepo(BasicConnection):
    table_name = 'mi_index_ix'


class MiIndexStRepo(BasicConnection):
    table_name = 'mi_index_st'


class MiIndexSumRepo(BasicConnection):
    table_name = 'mi_index_sum'


class MiIndexTriRepo(BasicConnection):
    table_name = 'mi_index_tri'
