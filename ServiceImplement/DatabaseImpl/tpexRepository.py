from Extends.database import BasicConnection


class Tpex3ItRepo(BasicConnection):
    table_name = 'tpex_3it'


class TpexIndex(BasicConnection):
    table_name = 'tpex_index'
