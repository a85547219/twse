from Extends.database import BasicConnection
import pymongo


class EmailRecordRepo(BasicConnection):
    table_name = 'email_record'

    def save_record(self, _to, _subject, _content, _datetime):
        self.collection.insert_one(
            {"to": _to, "subject": _subject, "content": _content, "datetime": _datetime})

    def get_lasted_data(self):
        cursor = self.collection.find({}).sort("datetime", pymongo.DESCENDING).limit(1)
        return cursor[0] if cursor.count() > 0 else None
