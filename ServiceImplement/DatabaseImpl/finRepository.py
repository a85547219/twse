from Extends.database import BasicConnection


class MonRepo(BasicConnection):
    table_name = 'mon'

    def get_by_sn(self, sn):
        return [data for data in self.collection.find({'SN': sn},
                                                      {'_id': 0, 'Y': 1, 'M': 1, 'MoM': 1, 'YoY': 1, 'AoA': 1,
                                                       'ACC_M_REV': 1}).sort([('Y', 1), ('M', 1)])]

    def get_after_date(self, date):
        name_dict = {}
        for data in self.collection.find({'updateTime': {"$gt": date}},
                                         {'_id': 0, 'Y': 1, 'M': 1, 'SN': 1, 'MoM': 1, 'YoY': 1, 'AoA': 1,
                                          'NOTE': 1}).sort([('Y', 1), ('M', 1)]):
            data_row = name_dict.get(data['SN'], [])
            name_dict.update()
            data_row.append(data)
            name_dict[data['SN']] = data_row

        return name_dict


class QuaRepo(BasicConnection):
    table_name = 'qua'

    def get_after_date(self, date):
        pass

    def get_by_sn(self, sn):
        return [data for data in self.collection.find({'SN': int(sn)},
                                                      {'_id': 0, 'Y': 1, 'Q': 1, 'EPS': 1}).sort([('Y', 1), ('Q', 1)])]
