from Extends.database import BasicConnection
from functools import lru_cache

from commonCode import CategoryEnum


class StockNoTable(BasicConnection):
    table_name = 'StockNoTable'

    @lru_cache(maxsize=32)
    def get_map(self):
        """
        :return: {'代號': {'NAME': '名稱','CATEGORY': '分類'}}
        """
        map_collection = {}
        for data in self.collection.find({}, {'_id': 0, 'SN': 1, 'CATEGORY': 1, 'NAME': 1}):
            map_collection[data['SN']] = {
                'NAME': data['NAME'],
                'CATEGORY': data['CATEGORY']
            }

        return map_collection

    def get_category(self, sn):
        return CategoryEnum(self.get_map().get(sn, {'CATEGORY': ''})['CATEGORY'])
