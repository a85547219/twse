from Extends.database import BasicConnection


class Twtb4uRepo(BasicConnection):
    table_name = 'twtb4u'

    def find_by_date(self, date):
        return {doc['SN']: doc['DT'] for doc in self.collection.find({'DATE': date}, {'_id': 0, 'SN': 1, 'DT': 1})}


class Twtb4uMsRepo(BasicConnection):
    table_name = 'twtb4u_ms'
