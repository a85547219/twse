from Extends.analysis import Trace
from commonCode import *


class KdStrategy(Trace):
    from_days = 380

    # other
    _hist_is_positive = False
    _kd_is_positive = False
    _macd_cross_price = 99999
    _days_after_macd_cross = 0

    def after_success_sell(self, price, doc):
        self._hist_is_positive = False
        self._kd_is_positive = False

    def after_success_buy(self, price, doc):
        pass

    def buy_or_sell(self, doc):
        self._reason = []
        self._note = ["現價:{}, 預估:{}, 量:{}, K:{}, D:{}, MACD:{}".format(
            round(doc[IndexEnum.CLOSE], 2),
            round(doc[IndexEnum.PREDICT_CROSS_PRICE], 2),
            doc[IndexEnum.VOL].astype(int),
            round(doc[IndexEnum.K], 3),
            round(doc[IndexEnum.D], 3),
            round(doc[IndexEnum.MACD], 3))]

        def record_doc():
            self._hist_is_positive = (doc[IndexEnum.HIST] >= 0 or self._hist_is_positive)
            self._kd_is_positive = doc[TechEnum.KD_CROSS] in (CrossOverEnum.GOLD_CROSS,
                                                              CrossOverEnum.AFTER_G) or self._kd_is_positive

        def notice_buy():
            def kd_condiction():
                return doc[IndexEnum.K] < 25

            return kd_condiction()

        def buy():
            def _macd_condiction():
                if doc[IndexEnum.MACD] > 0 and 1.1 > doc[TechEnum.MACD_POINT] / doc[IndexEnum.MACD] > 0.9:
                    self._reason.append("平穩。嘗試".format(round(doc[IndexEnum.MACD], 3)))
                    return True
                elif doc[IndexEnum.MACD] < 0:
                    self._reason.append("跌深。卡位".format(round(doc[IndexEnum.MACD], 3)))
                    return True
                else:
                    return False

            def _kd_condiction():
                if doc[IndexEnum.D] < 15:
                    self._reason.append("賣超。搶短".format(round(doc[IndexEnum.D], 3)))
                    return True

            return _macd_condiction() or _kd_condiction()

        def basic_filter():
            def vol_filter():
                return doc[IndexEnum.VOL] < 2000

            return vol_filter()

        if basic_filter():
            return TradeAction.DO_NOTHING, None
        elif notice_buy():
            record_doc()
            if buy():
                return TradeAction.BUY, doc[IndexEnum.PREDICT_CROSS_PRICE]
            return TradeAction.NOTICE_BUY, None
        else:
            return TradeAction.DO_NOTHING, None

    @staticmethod
    def comment():
        point_instructions = "N: 收盤價，P: 預測KD交叉時之現價。"
        buy_strategy_instructions = "關注買入: K<25。建議買入: MACD<0 或 D<15 或 MACD>0且平穩。 強烈建議買入:無。"
        sell_strategy_instructions = "關注賣出: 無。 建議賣出: 無。 強烈建議賣出:MACD RSI DMI KD任意兩種指標以上呈現反轉。"
        basic_filter = "過濾: 量>1000"
        return [point_instructions, buy_strategy_instructions, sell_strategy_instructions, basic_filter]
