from Extends.analysis import Trace
from commonCode import *


class rushStrategy(Trace):

    def after_success_sell(self, price, index_doc):
        pass

    def after_success_buy(self, price, index_doc):
        pass

    def buy_or_sell(self, index_doc):
        if index_doc[TechEnum.KD_CROSS] == CrossOverEnum.GOLD_CROSS:
            return TradeAction.MUST_BUY, index_doc[IndexEnum.LOW]
        elif index_doc[TechEnum.KD_CROSS] == CrossOverEnum.DEAD_CROSS:
            return TradeAction.MUST_SELL, index_doc[IndexEnum.HIGH]
        else:
            return TradeAction.DO_NOTHING, None

    def record_function(self, doc):
        if doc[TechEnum.KD_CROSS] == CrossOverEnum.DEAD_CROSS:
            col = self.record.get(RecordEnum.KD)
            col.append((CrossOverEnum.DEAD_CROSS, doc[IndexEnum.DATE], doc[IndexEnum.CLOSE], doc[TechEnum.KD_POINT]))
        elif doc[TechEnum.KD_CROSS] == CrossOverEnum.GOLD_CROSS:
            col = self.record.get(RecordEnum.KD)
            col.append((CrossOverEnum.GOLD_CROSS, doc[IndexEnum.DATE], doc[IndexEnum.CLOSE], doc[TechEnum.KD_POINT]))
