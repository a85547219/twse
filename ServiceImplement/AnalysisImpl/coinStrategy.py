from Extends.analysis import Trace
from commonCode import *


class CoinStrategy(Trace):
    from_days = 365
    _buy_price = []
    _is_over_threshold = False
    keep_buy_day = 1

    def after_success_sell(self, price, index_doc):
        self._buy_price = []
        self._is_over_threshold = False
        self.keep_buy_day = 1

    def after_success_buy(self, price, index_doc):
        self.keep_buy_day += 1
        self._buy_price.append(price)

    def record_function(self, doc):
        if self._buy_price and doc[IndexEnum.CLOSE] > (sum(self._buy_price) / len(self._buy_price)) * 1.05:
            self._is_over_threshold = True

    def buy_or_sell(self, doc):
        if doc[IndexEnum.VOL] < 1000:
            return

        self._note = ["現價:{}, 量:{}, K:{}, D:{}, P:{}, 5T:{}, 次數:{}".format(
            round(doc[IndexEnum.CLOSE], 2),
            doc[IndexEnum.VOL].astype(int),
            round(doc[IndexEnum.K], 2),
            round(doc[IndexEnum.D], 2),
            doc[IndexEnum.CLOSE], round(doc[IndexEnum.MA5], 2), self.keep_buy_day)]
        if doc[IndexEnum.MA20] > doc[IndexEnum.MA10] > doc[IndexEnum.MA5] and doc[IndexEnum.CLOSE] > doc[IndexEnum.MA5]:
            return TradeAction.MUST_BUY, (doc[IndexEnum.CLOSE] + doc[IndexEnum.OPEN]) / 2
        if self._is_over_threshold and doc[IndexEnum.CLOSE] < doc[IndexEnum.MA5]:
            return TradeAction.MUST_SELL, (doc[IndexEnum.CLOSE] + doc[IndexEnum.OPEN]) / 2

    def trade_reason(self):
        return self._reason, self._note

    @staticmethod
    def comment():
        return ["均線20>10>5，收盤>均線5"]
