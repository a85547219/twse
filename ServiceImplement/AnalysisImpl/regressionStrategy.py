from Extends.analysis import Trace
from commonCode import *


class RegressionStrategy(Trace):
    def after_success_sell(self, price, index_doc):
        pass

    def after_success_buy(self, price, index_doc):
        pass

    def buy_or_sell(self, index_doc):
        self._note = ["價: {}".format(index_doc[IndexEnum.CLOSE])]
        if index_doc[IndexEnum.VOL] < 2000 or index_doc[IndexEnum.IT] < 1:
            return TradeAction.DO_NOTHING, None

        if index_doc[IndexEnum.RT] is not 0:
            self._note.append(
                "法:{} 投:{} 慘:{} 量:{}".format(index_doc[IndexEnum.RT], index_doc[IndexEnum.IT],
                                             index_doc[IndexEnum.VOL].astype(int) - index_doc[IndexEnum.RT],
                                             index_doc[IndexEnum.VOL].astype(int)))
        if index_doc[IndexEnum.MA5] < index_doc[IndexEnum.CLOSE] < index_doc[IndexEnum.MA20]:
            return TradeAction.BUY, index_doc[IndexEnum.CLOSE]
        return TradeAction.DO_NOTHING, None
