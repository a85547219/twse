from Extends.parser import Spider
from datetime import datetime


class T86(Spider):
    """三大法人買賣超日報
    http://www.twse.com.tw/fund/T86?response=json&date=20180411&selectType=ALLBUT0999
    20120502->20141128 :
    ["證券代號","證券名稱","外資買進股數","外資賣出股數","外資買賣超股數","投信買進股數","投信賣出股數","投信買賣超股數","自營商買賣超股數","自營商買進股數","自營商賣出股數","三大法人買賣超股數"]
    ["2303","聯電            ","46,911,000","28,499,762","18,411,238","5,707,000","0","5,707,000","646,000","648,000","2,000","24,764,238"]
    20141201->20171215 :
    ["證券代號","證券名稱","外資買進股數","外資賣出股數","外資買賣超股數","投信買進股數","投信賣出股數","投信買賣超股數","自營商買賣超股數","自營商買進股數(自行買賣)","自營商賣出股數(自行買賣)","自營商買賣超股數(自行買賣)","自營商買進股數(避險)","自營商賣出股數(避險)","自營商買賣超股數(避險)","三大法人買賣超股數"]
    ["00632R","元大台灣50反1   ","18,769,000","148,000","18,621,000","0","0","0","-3,354,000","960,000","1,000,000","-40,000","3,080,000","6,394,000","-3,314,000","15,267,000"]
    20171218->20190605:
    ["證券代號","證券名稱","外陸資買進股數(不含外資自營商)","外陸資賣出股數(不含外資自營商)","外陸資買賣超股數(不含外資自營商)","外資自營商買進股數","外資自營商賣出股數","外資自營商買賣超股數","投信買進股數","投信賣出股數","投信買賣超股數","自營商買賣超股數","自營商買進股數(自行買賣)","自營商賣出股數(自行買賣)","自營商買賣超股數(自行買賣)","自營商買進股數(避險)","自營商賣出股數(避險)","自營商買賣超股數(避險)","三大法人買賣超股數"]
    ["2303","聯電            ","107,591,232","64,781,700","42,809,532","0","0","0","0","0","0","-246,598","188,000","400,000","-212,000","455,000","489,598","-34,598","42,562,934"]
    """

    table_name = 'ii'
    url = 'http://www.twse.com.tw/fund/T86'
    params = {
        'selectType': 'ALLBUT0999'
    }
    ver2_start_date = datetime(2014, 12, 1)
    ver3_start_date = datetime(2017, 12, 18)

    def extract(self, content):
        def ver1():
            raw_data = content['data']
            db_data = []
            for row in raw_data:
                record = {'DATE': self.date,
                          'SN': row[0],
                          'FIDL_B': int(row[2].replace(',', "")) // 1000,
                          'FIDL_S': int(row[3].replace(',', "")) // 1000,
                          'FIDL': int(row[4].replace(',', "")) // 1000,
                          'IT_B': int(row[5].replace(',', "")) // 1000,
                          'IT_S': int(row[6].replace(',', "")) // 1000,
                          'IT': int(row[7].replace(',', "")) // 1000,
                          'DL_B(SD)': int(row[8].replace(',', "")) // 1000,
                          'DL_S(SD)': int(row[9].replace(',', "")) // 1000,
                          'DL_SD': int(row[10].replace(',', "")) // 1000,
                          'II': int(row[11].replace(',', "")) // 1000
                          }
                buy = record['FIDL_B'] + record['IT_B'] + record['DL_B(SD)']
                sell = record['FIDL_S'] + record['IT_S'] + record['DL_S(SD)']
                record['RT'] = max(buy, sell)
                db_data.append(record)
            return db_data

        def ver2():
            raw_data = content['data']
            db_data = []
            for row in raw_data:
                record = {'DATE': self.date,
                          'SN': row[0],
                          'FIDL_B': int(row[2].replace(',', "")) // 1000,
                          'FIDL_S': int(row[3].replace(',', "")) // 1000,
                          'FIDL': int(row[4].replace(',', "")) // 1000,
                          'IT_B': int(row[5].replace(',', "")) // 1000,
                          'IT_S': int(row[6].replace(',', "")) // 1000,
                          'IT': int(row[7].replace(',', "")) // 1000,
                          'DL': int(row[8].replace(',', "")) // 1000,
                          'DL_B(SD)': int(row[9].replace(',', "")) // 1000,
                          'DL_S(SD)': int(row[10].replace(',', "")) // 1000,
                          'DL_SD': int(row[11].replace(',', "")) // 1000,
                          'DL_B(HG)': int(row[12].replace(',', "")) // 1000,
                          'DL_S(HG)': int(row[13].replace(',', "")) // 1000,
                          'DL_HG': int(row[14].replace(',', "")) // 1000,
                          'II': int(row[15].replace(',', "")) // 1000
                          }
                buy = record['FIDL_B'] + record['IT_B'] + record['DL_B(SD)'] + record['DL_B(HG)']
                sell = record['FIDL_S'] + record['IT_S'] + record['DL_S(SD)'] + record['DL_S(HG)']
                record['RT'] = max(buy, sell)
                db_data.append(record)
            return db_data

        def ver3():
            raw_data = content['data']
            db_data = []
            for row in raw_data:
                record = {'DATE': self.date,
                          'SN': row[0],
                          'CI_B': int(row[2].replace(',', "")) // 1000,
                          'CI_S': int(row[3].replace(',', "")) // 1000,
                          'CI': int(row[4].replace(',', "")) // 1000,
                          'FIDL_B': int(str(row[5]).replace(',', "")) // 1000,
                          'FIDL_S': int(str(row[6]).replace(',', "")) // 1000,
                          'FIDL': int(str(row[7]).replace(',', "")) // 1000,
                          'IT_B': int(row[8].replace(',', "")) // 1000,
                          'IT_S': int(row[9].replace(',', "")) // 1000,
                          'IT': int(row[10].replace(',', "")) // 1000,
                          'DL': int(row[11].replace(',', "")) // 1000,
                          'DL_B(SD)': int(row[12].replace(',', "")) // 1000,
                          'DL_S(SD)': int(row[13].replace(',', "")) // 1000,
                          'DL_SD': int(row[14].replace(',', "")) // 1000,
                          'DL_B(HG)': int(row[15].replace(',', "")) // 1000,
                          'DL_S(HG)': int(row[16].replace(',', "")) // 1000,
                          'DL_HG': int(row[17].replace(',', "")) // 1000,
                          'II': int(row[18].replace(',', "")) // 1000
                          }
                buy = record['CI_B'] + record['FIDL_B'] + record['IT_B'] + record['DL_B(SD)'] + record['DL_B(HG)']
                sell = record['CI_S'] + record['FIDL_S'] + record['IT_S'] + record['DL_S(SD)'] + record['DL_S(HG)']
                record['RT'] = max(buy, sell)
                db_data.append(record)
            return db_data

        if self.date < self.ver2_start_date:
            return ver1()
        elif self.date < self.ver3_start_date:
            return ver2()
        else:
            return ver3()


class BFI82U(Spider):
    """ 三大法人買賣金額統計表
         http://www.twse.com.tw/fund/BFI82U?response=json&dayDate=20190526&type=ALL&connection=close&_1559119528195
        ["單位名稱","買進金額","賣出金額","買賣差額"]
        ["自營商(自行買賣)","1,734,938,660","2,183,051,347","-448,112,687"]
        """

    table_name = 'ii_sum'
    url = 'http://www.twse.com.tw/fund/BFI82U'
    params = {
        'type': 'ALL'
    }

    def common_setting(self):
        super().common_setting()
        self.params['dayDate'] = self.date_str

    def extract(self, content):
        raw_data = content['data']
        db_data = []
        for row in raw_data:
            record = {
                'DATE': self.date,
                'MCM': row[0],
                'B_A': int(row[1].replace(',', "")),
                'S_A': int(row[2].replace(',', "")),
                'B&S_A': int(row[3].replace(',', ""))
            }
            db_data.append(record)
        return db_data
