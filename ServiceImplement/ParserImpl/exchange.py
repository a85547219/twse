from pymongo import UpdateOne
import pandas as pd
from datetime import datetime
from Extends.parser import Spider


class MI(Spider):
    """
    https://www.twse.com.tw/exchangeReport/FMTQIK?response=json&date=20140101
    {"stat":"OK","date":"20200617","title":"109年06月市場成交資訊",
    "fields":["日期","成交股數","成交金額","成交筆數","發行量加權股價指數","漲跌點數"],
    "data":[["109/06/01","4,975,164,301","169,563,746,947","1,350,613","11,079.02","136.86"],
    ["109/06/02","4,974,734,421","182,409,278,902","1,399,684","11,127.93","48.91"],
    https://www.twse.com.tw/indicesReport/MI_5MINS_HIST?response=json&date=20200607
    {"stat":"OK","title":"109年01月 發行量加權股價指數歷史資料","date":"20200101",
    "fields":["日期","開盤指數","最高指數","最低指數","收盤指數"],
    "data":[["109/01/02","12,026.50","12,110.74","12,026.23","12,100.48"],
    ["109/01/03","12,167.44","12,197.64","12,023.60","12,110.43"],
    ["109/01/06","12,035.71","12,040.08","11,953.36","11,953.36"],
    ["109/01/07","11,961.97","11,986.03","11,822.40","11,880.32"],
    ["109/01/08","11,818.76","11,899.67","11,777.45","11,817.10"],
    ["109/01/09","11,889.46","11,992.55","11,889.46","11,970.63"],["109/01/10","12,009.27","12,038.21","11,959.06","12,024.65"],["109/01/13","12,069.61","12,113.42","12,037.24","12,113.42"],["109/01/14","12,161.73","12,186.62","12,140.27","12,179.81"],["109/01/15","12,169.92","12,169.92","12,048.01","12,091.88"],["109/01/16","12,006.08","12,075.54","12,006.08","12,066.93"],["109/01/17","12,080.72","12,117.55","12,055.91","12,090.29"],["109/01/20","12,107.56","12,151.42","12,101.55","12,118.71"],["109/01/30","11,933.23","11,933.23","11,418.22","11,421.74"],["109/01/31","11,494.03","11,594.21","11,436.95","11,495.10"]]}
    """
    table_name = 'mi'
    url = ['https://www.twse.com.tw/indicesReport/MI_5MINS_HIST',
           'https://www.twse.com.tw/exchangeReport/FMTQIK']
    params = {
        'date': '20200101'
    }

    def check_status(self, pages):
        for page in pages:
            content = page.json()
            if content['stat'] != 'OK':
                return None
        return pages

    def extract(self, contents):
        mi_collection = contents[0].json()["data"]
        fm_collection = contents[1].json()["data"]
        if len(mi_collection) != len(fm_collection):
            return {}

        mi, fm = mi_collection[len(mi_collection) - 1], fm_collection[len(fm_collection) - 1]
        date = datetime.strptime(str(int(mi[0][0:3]) + 1911) + mi[0][3:], '%Y/%m/%d')
        data_dict = {'DATE': date,
                     'O': float(mi[1].replace(',', "")),
                     'H': float(mi[2].replace(',', "")),
                     'L': float(mi[3].replace(',', "")),
                     'C': float(mi[4].replace(',', "")),
                     'V': int(fm[1].replace(',', "")),
                     'A': int(fm[2].replace(',', "")),
                     'T': int(fm[3].replace(',', "")),
                     'F': float(fm[5].replace(',', ""))}

        return data_dict

    def save(self, content):
        self.get_collection(self.table_name).insert_one(content)


class MI_INDEX(Spider):
    """每日收盤行情-統計
        Data1
        ["指數","收盤指數","漲跌(+/-)","漲跌點數","漲跌百分比(%)","特殊處理註記"]
        ["寶島股價指數","12,541.18","<p style ='color:green'>-<\u002fp>","104.81","-0.83",""]
        http://www.twse.com.tw/exchangeReport/MI_INDEX?response=json&type=ALLBUT0999&date=20190426
        Data3:
        ["成交統計","成交金額(元)","成交股數(股)","成交筆數"]
       ["1.一般股票","103,520,514,205","2,205,138,480","836,298"]
       Data4
       ["類型","整體市場","股票"]
       ["上漲(漲停)","3,709(10)","663(7)"],["下跌(跌停)","1,850(16)","147(0)"],["持平","294","58"],["未成交","3,749","3"],["無比價","875","6"]
       Data5
       ["證券代號","證券名稱","成交股數","成交筆數","成交金額","開盤價","最高價","最低價","收盤價","漲跌(+/-)","漲跌價差","最後揭示買價","最後揭示買量","最後揭示賣價","最後揭示賣量","本益比"]
        ["0050","元大台灣50","3,815,090","2,520","314,520,959","82.80","82.80","82.20","82.45","<p style= color:green>-<\u002fp>","0.95","82.40","25","82.45","37","0.00"]
    """

    table_name = 'mi_index'
    url = 'http://www.twse.com.tw/exchangeReport/MI_INDEX'
    params = {
        'type': 'ALLBUT0999'
    }

    def extract(self, content):
        def get_data1(raw_data):
            db_data = []
            for row in raw_data:
                record = {'DATE': self.date,
                          'SN': row[0],
                          'IX': float(row[1].replace(',', "")) if '-' not in row[1] else None}
                if '>' in row[2]:
                    am = None if '<' not in row[2] else row[2].split('>')[1][0]
                    record['UD'] = float(am + row[3].replace(',', "")) if '--' not in row[3] else None
                    record['RATE'] = float(row[4]) if '--' not in row[4] else None
                else:
                    record['UD'] = None
                    record['RATE'] = None
                db_data.append(record)
            return db_data

        def get_data3(raw_data):
            data_element = []
            for row in raw_data:
                record = {
                    'DATE': self.date,
                    'SN': row[0]
                }
                if row[1] == 0:
                    record['A'] = 0.0
                    record['V'] = 0.0
                    record['T'] = 0.0
                else:
                    record['A'] = int(row[1].replace(',', ""))
                    record['V'] = int(row[2].replace(',', "")) // 1000
                    record['T'] = int(row[3].replace(',', ""))
                data_element.append(record)
            return data_element

        def get_data4(raw_data):
            record = {'DATE': self.date}
            ap = raw_data[0][1].split('(')
            s = raw_data[0][2].split('(')
            record['WM_U'] = int(ap[0].replace(',', ''))
            record['WM_LU'] = int(ap[1].replace(',', '').replace(')', ''))
            record['U'] = int(s[0].replace(',', ''))
            record['LU'] = int(s[1].replace(',', '').replace(')', ''))

            ap = raw_data[1][1].split('(')
            s = raw_data[1][2].split('(')
            record['WM_D'] = int(ap[0].replace(',', ''))
            record['WM_LD'] = int(ap[1].replace(',', '').replace(')', ''))
            record['D'] = int(s[0].replace(',', ''))
            record['LD'] = int(s[1].replace(',', '').replace(')', ''))

            record['WM_F'] = int(raw_data[2][1].replace(',', ''))
            record['F'] = int(raw_data[2][2].replace(',', ''))

            record['WM_US'] = int(raw_data[3][1].replace(',', ''))
            record['US'] = int(raw_data[3][2].replace(',', ''))

            record['WM_NC'] = int(raw_data[4][1].replace(',', ''))
            record['NC'] = int(raw_data[4][2].replace(',', ''))
            return record

        def get_data5(raw_data):
            db_data = []
            for row in raw_data:
                am = row[9] if '<' not in row[9] else row[9].split('>')[1][0]
                record = {'DATE': self.date,
                          'SN': row[0],
                          'V': round(int(row[2].replace(',', "")) / 1000, 0),
                          'T': int(row[3].replace(',', "")),
                          'A': int(row[4].replace(',', "")),
                          'UD': float(am + row[10].replace(',', "")) if am == "+" or am == "-" else 0.0,
                          'PE': float(row[15].replace(",", "")) if row[15] != "" else "",
                          }
                if '--' in row[5]:
                    record['ACT'] = False
                else:
                    record['O'] = float(row[5].replace(',', ""))
                    record['H'] = float(row[6].replace(',', ""))
                    record['L'] = float(row[7].replace(',', ""))
                    record['C'] = float(row[8].replace(',', ""))
                    record['MAP'] = round((record['H'] - record['L']) / record['O'], 3)
                    record['AP'] = round((record['O'] - record['C']) / record['O'], 3)
                    record['ACT'] = True
                db_data.append(record)
            return db_data

        return {'data1': get_data1(content['data1']),
                'data2': get_data1(content['data2']),
                'data3': get_data1(content['data3']),
                'data4': get_data1(content['data4']),
                'data5': get_data1(content['data5']),
                'data6': get_data1(content['data6']),
                'data7': get_data3(content['data7']),
                'data8': get_data4(content['data8']),
                'data9': get_data5(content['data9'])}

    def save(self, content):
        self.get_collection(self.table_name + '_ix').insert_many(content['data1'])
        self.get_collection(self.table_name + '_ix').insert_many(content['data2'])
        self.get_collection(self.table_name + '_ix').insert_many(content['data3'])
        self.get_collection(self.table_name + '_tri').insert_many(content['data4'])
        self.get_collection(self.table_name + '_tri').insert_many(content['data5'])
        self.get_collection(self.table_name + '_tri').insert_many(content['data6'])
        self.get_collection(self.table_name + '_sum').insert_many(content['data7'])
        self.get_collection(self.table_name + '_st').insert_one(content['data8'])
        self.get_collection(self.table_name).insert_many(content['data9'])


class MARGN(Spider):
    """信用交易統計
        http://www.twse.com.tw/exchangeReport/MI_MARGN?response=json&date=20190520&selectType=ALL
        data:
         ["股票代號","股票名稱","買進","賣出","現金償還","前日餘額","今日餘額","限額","買進","賣出","現券償還","前日餘額","今日餘額","限額","資券互抵","註記"]
        ["0050","元大台灣50","126","462","20","2,006","1,650","192,250","3","24","0","390","411","192,250","1",""]
        creditList:
        ["項目","買進","賣出","現金(券)償還","前日餘額","今日餘額"]
         ["融資(交易單位)","161,786","162,842","4,901","7,088,172","7,082,215"]
    """

    table_name = 'margin'
    url = 'http://www.twse.com.tw/exchangeReport/MI_MARGN'
    params = {
        'selectType': 'ALL'
    }

    def extract(self, content):

        def get_data():
            raw_data = content['data']
            db_data = []

            for row in raw_data:
                record = {
                    'DATE': self.date,
                    'SN': row[0],
                    'FN_B': int(row[2].replace(',', "")) // 1000,
                    'FN_S': int(row[3].replace(',', "")) // 1000,
                    'FN_A_GB': int(row[4].replace(',', "")) // 1000,
                    'FN_YD_R': int(row[5].replace(',', "")) // 1000,
                    'FN_ND_R': int(row[6].replace(',', "")) // 1000,
                    'FN_LIM': int(row[7].replace(',', "")) // 1000,
                    'MN_B': int(row[8].replace(',', "")) // 1000,
                    'MN_S': int(row[9].replace(',', "")) // 1000,
                    'MN_V_GB': int(row[10].replace(',', "")) // 1000,
                    'MN_YD_R': int(row[11].replace(',', "")) // 1000,
                    'MN_ND_R': int(row[12].replace(',', "")) // 1000,
                    'MN_LIM': int(row[13].replace(',', "")) // 1000,
                    'FM': int(row[14].replace(',', "")) // 1000,
                    'NOTE': row[15]
                }
                db_data.append(record)
            return db_data

        def get_credit_list():
            raw_data = content['creditList']
            db_data = []
            for row in raw_data:
                record = {
                    'DATE': self.date,
                    'MM': row[0],
                    'B': int(row[2].replace(',', "")) // 1000,
                    'S': int(row[3].replace(',', "")) // 1000,
                    'A_GB': int(row[4].replace(',', "")) // 1000,
                    'YD_R': int(row[5].replace(',', "")) // 1000,
                    'ND_R': int(row[6].replace(',', "")) // 1000
                }
                db_data.append(record)
            return db_data

        return {'data': get_data(), 'creditList': get_credit_list()}

    def save(self, content):
        self.get_collection(self.table_name).insert_many(content['data'])
        self.get_collection(self.table_name + '_ms').insert_many(content['creditList'])


class TWTB4U(Spider):
    """當日沖銷交易標的及成交量值
            http://www.twse.com.tw/exchangeReport/TWTB4U?response=json&date=20200407&selectType=All
           data:
           ["證券代號","當日沖銷交易成交股數","當日沖銷交易買進成交金額","當日沖銷交易賣出成交金額"]
            ["0050","元大台灣50","","543,000","42,583,850","42,618,400"]
            creditList:
            ["當日沖銷交易總成交股數","當日沖銷交易總成交股數占市場比重%","當日沖銷交易總買進成交金額","當日沖銷交易總買進成交金額占市場比重%","當日沖銷交易總賣出成交金額","當日沖銷交易總賣出成交金額占市場比重%"]
            ["602,336,000","15.07","31,538,181,850","29.02","31,584,966,940","29.07"]
        """

    table_name = 'twtb4u'
    url = 'http://www.twse.com.tw/exchangeReport/TWTB4U'
    params = {
        'selectType': 'All'
    }

    def extract(self, content):
        def get_data():
            raw_data = content['data']
            db_data = []
            for row in raw_data:
                record = {
                    'DATE': self.date,
                    'SN': row[0],
                    'DT': int(row[3].replace(',', "")) // 1000,
                    'DT_B_A': round(int(row[4].replace(',', ""))),
                    'DT_S_A': round(int(row[5].replace(',', "")))
                }
                db_data.append(record)
            return db_data

        def get_credit_list():
            raw_data = content['creditList']
            for row in raw_data:
                return {
                    'DATE': self.date,
                    'DT': int(row[0].replace(',', "")) // 1000,
                    'RATE': float(row[1].replace(',', "")),
                    'DT_B_A': int(row[2].replace(',', "")),
                    'DT_B_RATE': float(row[3].replace(',', "")),
                    'DT_S_A': int(row[4].replace(',', "")),
                    'DT_S_RATE': float(row[5].replace(',', ""))
                }

        return {'data': get_data(),
                'creditList': get_credit_list()}

    def save(self, content):
        self.get_collection(self.table_name).insert_many(content['data'])
        self.get_collection(self.table_name + '_ms').insert_one(content['creditList'])


class TWT93U(Spider):
    """融券借券
            http://www.twse.com.tw/exchangeReport/TWT93U?response=json&date=20190425
            [["股票代號","股票名稱","前日餘額","賣出","買進","現券","今日餘額","限額"], ["前日餘額","當日賣出","當日還券","當日調整","當日餘額","次一營業日可限額"],"備註"]
            ["0050","元大台灣50","585,000","0","0","0","585,000","206,875,000","1,239,000","0","0","0","1,239,000","2,598,708",""]
        """

    table_name = 'twsasu'
    url = 'http://www.twse.com.tw/exchangeReport/TWT93U'

    def extract(self, content):
        raw_data = content['data']
        db_data = []

        for row in raw_data:
            record = {
                'DATE': self.date,
                'SN': row[0],
                'MN_YD_R': int(row[2].replace(',', "")) // 1000,
                'MN_S': int(row[3].replace(',', "")) // 1000,
                'MN_B': int(row[4].replace(',', "")) // 1000,
                'MN_SP': int(row[5].replace(',', "")) // 1000,
                'MN_ND_R': int(row[6].replace(',', "")) // 1000,
                'MN_LIM': int(row[7].replace(',', "")) // 1000,
                'SL_YD_R': int(row[8].replace(',', "")) // 1000,
                'SL_ND_S': int(row[9].replace(',', "")) // 1000,
                'SL_ND_GB': int(row[10].replace(',', "")) // 1000,
                'SL_ND_ADJ': int(row[11].replace(',', "")) // 1000,
                'SL_ND_R': int(row[12].replace(',', "")) // 1000,
                'SL_TD_LIM': int(row[13].replace(',', "")) // 1000,
                'NOTE': row[14]
            }
            db_data.append(record)
        return db_data


class TWT72U(Spider):
    """借券餘額
        http://www.twse.com.tw/exchangeReport/TWT72U?response=json&date=20190521&selectType=SLBNLB
         ["證券代號","證券名稱","前日借券餘額(1)股","本日異動股借券(2)","本日異動股還券(3)","本日借券餘額股(4)=(1)+(2)-(3)","本日收盤價(5)單位：元","借券餘額市值單位：元(6)=(4)*(5)","市場別"]
        ["2359","所羅門","1,662,000","0","0","1,662,000","21.85","36,314,700","集中市場"]
        """

    table_name = 'twt72u'
    url = 'http://www.twse.com.tw/exchangeReport/TWT72U'

    def extract(self, content):
        raw_data = content['data']
        db_data = []

        for row in raw_data:
            record = {
                'DATE': self.date,
                'SN': row[0],
                'YD_SL_R': int(row[2].replace(',', "")) // 1000,
                'ND_SS_SL': int(row[3].replace(',', "")) // 1000,
                'ND_SS_GB': int(row[4].replace(',', "")) // 1000,
                'ND_SL_R': int(row[5].replace(',', "")) // 1000,
                'C': float(row[6].replace(',', "")),
                'R_A': int(row[7].replace(',', "")),
                'MKT': row[8]
            }
            db_data.append(record)
        return db_data


class TWTASU(Spider):
    """日融券賣出與借券賣出成交量值
        證券名稱	融券賣出成交數量	融券賣出成交金額	借券賣出成交數量	借券賣出成交金額
            1229 聯華	            6	                  213,700	                    16	                            569,650
         http://www.twse.com.tw/exchangeReport/TWTASU?response=json&date=20190520
        """
    table_name = 'twsasu'
    url = 'http://www.twse.com.tw/exchangeReport/TWTASU'
    params = {
        'type': 'ALLBUT0999'
    }

    def extract(self, content):
        raw_data = content['data']
        db_data = []

        for row in raw_data:
            record = {
                'DATE': self.date,
                'SN': row[0].split(" ")[0],
                'MN_B_V': int(row[1].replace(',', "")) // 1000,
                'MN_B_A': int(row[2].replace(',', "")),
                'SL_S_V': int(row[3].replace(',', "")) // 1000,
                'Sl_S_A': int(row[4].replace(',', ""))
            }
            db_data.append(record)
        return db_data


class BWIBBU(Spider):
    """殖利率
        http://www.twse.com.tw/exchangeReport/BWIBBU_d?response=json&date=20190107&selectType=ALL
        ["證券代號","證券名稱","殖利率(%)","股利年度","本益比","股價淨值比","財報年/季"]
        ["2337","旺宏","6.00",107,"5.06","1.16","108/1"]
        """

    table_name = 'bwibbu'
    url = 'http://www.twse.com.tw/exchangeReport/BWIBBU_d'
    params = {
        'selectType': 'ALL'
    }

    def extract(self, content):

        raw_data = content['data']
        db_data = []

        for row in raw_data:
            record = {
                'DATE': self.date,
                'SN': row[0],
                'DY': float(row[2]),
                'YY': row[3],
                'PE': float(row[4].replace(',', '')) if row[4] != '-' else '',
                'PBR': float(row[5].replace(',', '')) if row[4] != '-' else '',
                'YY/QQ': row[6]
            }
            db_data.append(record)
        return db_data

    def save(self, content):
        update_list = []
        for row in content:
            update_list.append(UpdateOne({'SN': row['SN']}, {'$set': row}, upsert=True))
        self.get_collection().bulk_write(update_list)


class StockNoToName(Spider):
    # https://isin.twse.com.tw/isin/C_public.jsp?response=json&strMode=2
    table_name = 'StockNoTable'
    url = 'https://isin.twse.com.tw/isin/C_public.jsp'
    params = {
        'strMode': 2
    }

    def check_status(self, page):
        return pd.read_html(page.text)[0]

    def extract(self, content):
        db_data = []
        for raw in content[2:].get_values():
            str_split = raw[0].split('\u3000')
            if len(str_split) < 2:
                continue
            else:
                record = {
                    "SN": str_split[0],
                    "NAME": str_split[1],
                    "CATEGORY": "" if raw[4] != raw[4] else raw[4]
                }
            db_data.append(record)

        return db_data

    def save(self, content):
        update_list = []
        for row in content:
            update_list.append(UpdateOne({'SN': row['SN']}, {'$set': row}, upsert=True))
        self.get_collection().bulk_write(update_list)
