from Extends.parser import Spider
import ssl
import pandas as pd
import datetime
import csv
from urllib.request import urlopen


# 籌碼集中
class CYQ(Spider):
    """籌碼集中 (csv)
    TDCC官網 －＞ 下載專區 －＞ 開放資料專區－＞ 股務資訊 －＞ 股權分散表
        https://smart.tdcc.com.tw/opendata/getOD.ashx?id=1-5
    """

    table_name = 'cyq'
    url = 'https://smart.tdcc.com.tw/opendata/getOD.ashx?id=1-5'
    params = {}

    def extract(self, content):
        db_data = []
        data = {}
        count = 1

        for row in content.values:
            header = 'G' + str(count) + '_' if count != 17 else 'SUM_'
            data[header + 'P'] = int(row[3])
            data[header + 'V'] = int(row[4]) // 1000
            data[header + 'RATE'] = float(row[5])
            if count == 17:
                data['DATE'] = datetime.datetime.strptime(str(row[0]), "%Y%m%d")
                data['SN'] = row[1]
                db_data.append(data)
                data = {}
                count = 1
            else:
                count += 1

        return db_data

    def query(self):
        # info = urlopen(self.url, context=ssl.SSLContext()).read()
        # return csv.reader(info.decode('utf-8'))
        ssl._create_default_https_context = ssl._create_unverified_context
        return pd.read_csv(self.url)

    def check_status(self, page):
        return page
