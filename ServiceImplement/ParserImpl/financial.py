import pandas as pd
import requests
from io import StringIO
from Extends.parser import Spider
from datetime import datetime


class Mon(Spider):
    """
    https://mops.twse.com.tw/nas/t21/sii/t21sc03_109_5_0.html
    """
    table_name = 'mon'
    url = 'https://mops.twse.com.tw/nas/t21/sii/t21sc03_{}_{}_0.html'
    params = {}

    def __init__(self, year, month):
        super().__init__()
        self.year = year
        self.month = month
        self.url = self.url.format(self.year - 1911, self.month)

    def extract(self, content):
        dfs = pd.read_html(StringIO(content.text), encoding='big-5')

        df = pd.concat([df for df in dfs if 5 < df.shape[1] <= 11])

        if 'levels' in dir(df.columns):
            df.columns = df.columns.get_level_values(1)
        else:
            df = df[list(range(0, 10))]
            column_index = df.index[(df[0] == '公司代號')][0]
            df.columns = df.iloc[column_index]

        df['當月營收'] = pd.to_numeric(df['當月營收'], 'coerce')
        df = df[~df['當月營收'].isnull()]
        df = df[df['公司代號'] != '合計']

        df = df.drop(columns=['公司名稱'])

        df = df.rename(columns={'公司代號': 'SN',
                                '當月營收': 'REV',
                                '上月營收': 'L_M_REV',
                                '去年當月營收': 'L_Y_REV',
                                '上月比較增減(%)': 'MoM',
                                '去年同月增減(%)': 'YoY',
                                '當月累計營收': 'ACC_M_REV',
                                '去年累計營收': 'ACC_Y_REV',
                                '前期比較增減(%)': 'AoA',
                                '備註': 'NOTE'})
        # 補上時間財報時間
        df['Y'] = self.year
        df['M'] = self.month
        # 更新時間
        df['updateTime'] = datetime.now()
        # 重新排序，欄位
        col = df.columns.tolist()
        df = df[col[-2:] + col[:-2]]
        # 轉成可儲存格式
        db_data = df.to_dict('record')
        return db_data

    def check_status(self, page):
        page.encoding = 'big5'
        return page

    def save(self, content):
        exists_cursor = self.get_collection(self.table_name).find({'Y': self.year, 'M': self.month},
                                                                  {'_id': 0, 'SN': 1}).distinct('SN')
        items = [item for item in exists_cursor]
        db_content = [item for item in content if item['SN'] not in items]
        self.get_collection(self.table_name).insert_many(db_content)


class QUA(Spider):
    table_name = 'qua'
    url = 'https://mops.twse.com.tw/mops/web/t163sb04'
    params = {
        'encodeURIComponent': 1,
        'step': 1,
        'firstin': 1,
        'off': 1,
        'isQuery': 'Y',
        'TYPEK': 'sii',
        'year': 108,
        'season': 1
    }

    def __init__(self, year, season):
        super().__init__()
        self.year = year
        self.season = season
        self.params['year'] = year - 1911
        self.params['season'] = season

    def query(self):
        return requests.post(self.url, self.params)

    def check_status(self, page):
        return page

    def extract(self, content):
        dfs = pd.read_html(StringIO(content.text), encoding='utf-8')
        data = [df for df in dfs if self._equals('基本每股盈餘（元）', df.columns.values)]

        df = pd.concat(data)
        df = df.rename(columns={'公司代號': 'SN',
                                '基本每股盈餘（元）': 'EPS'})
        df['Y'] = self.year
        df['Q'] = self.season
        # 更新時間
        df['updateTime'] = datetime.now()
        db_data = df[['SN', 'EPS', 'Y', 'Q']].to_dict('record')
        return db_data

    def save(self, content):
        exists_cursor = self.get_collection(self.table_name).find({'Y': self.year, 'Q': self.season},
                                                                  {'_id': 0, 'SN': 1}).distinct('SN')
        items = [item for item in exists_cursor]
        db_content = [item for item in content if item['SN'] not in items]
        self.get_collection(self.table_name).insert_many(db_content)

    def _equals(self, target, columns_list) -> bool:
        for column in columns_list:
            if target == column:
                return True
        return False
