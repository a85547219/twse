from Extends.parser import Spider
import pandas as pd
from io import StringIO


class OTC(Spider):
    table_name = 'tpex_index'
    url = 'https://www.tpex.org.tw/web/stock/aftertrading/otc_quotes_no1430/stk_wn1430_result.php?l=zh-tw&o=htm&d={}/{}&se=EW&s=0,asc,0'
    params = {
    }

    def __init__(self, date):
        super().__init__(date)
        self.url = self.url.format(date.year - 1911, self.date.strftime("%m/%d"))

    def common_setting(self):
        pass

    def check_status(self, pages):
        return pages

    def extract(self, contents):
        dfs = pd.read_html(StringIO(contents.text), encoding='big-5')
        df = dfs[0]

        df.columns = df.columns.get_level_values(1)
        df = df.drop(columns=['名稱'])
        df = df.rename(columns={
            '代號': 'SN',
            '成交筆數': 'T',
            '成交股數': 'V',
            '收盤': 'C',
            '最低': 'L',
            '最後買價': 'LAST_BUY_PRICE',
            '最後賣價': 'LAST_SELL_PRICE',
            '最高': 'H',
            '次日漲停價': 'NEXT_MAX_PRICE',
            '次日跌停價': 'NEXT_MIN_PRICE',
            '成交金額(元)': 'A',
            '最後買量(千股)': 'LAST_BUY_VOLUME',
            '最後賣量(千股)': 'LAST_SELL_VOLUME',
            '漲跌': 'AP',
            '發行股數': 'TOTAL_VOLUME',
            '開盤': 'O',
        })

        df['DATE'] = self.date
        db_data = df.to_dict('record')
        return db_data

    def save(self, content):
        if len(content) == 1:
            return
        self.get_collection(self.table_name).insert_many(content)


class ItTrade(Spider):
    table_name = 'tpex_3it'
    url = 'https://www.tpex.org.tw/web/stock/3insti/daily_trade/3itrade_hedge_result.php?l=zh-tw&o=htm&se=EW&t=D&d={}/{}&s=0,asc'
    params = {

    }

    def __init__(self, date):
        super().__init__(date)
        self.url = self.url.format(date.year - 1911, self.date.strftime("%m/%d"))

    def common_setting(self):
        pass

    def check_status(self, pages):
        return pages

    def extract(self, contents):
        dfs = pd.read_html(StringIO(contents.text), encoding='big-5')
        df = dfs[0]

        df.columns = dfs[0].columns.get_level_values(1) + dfs[0].columns.get_level_values(2)
        # df = df.drop(columns=['名稱', 'Unnamed: 24_level_1','自營商'])

        # df = df.rename(columns={
        #     '代號': 'SN',
        #     '外資及陸資買股數': 'FIDL_B',
        #     '外資及陸資賣股數': 'FIDL_S',
        #     '外資及陸資淨買股數': 'FIDL',
        #     '投信買股數': 'IT_B',
        #     '投信賣股數': 'IT_S',
        #     '投信淨買股數': 'IT',
        #     '自營商淨買股數': 'DL',
        #     '自營商(自行買賣)買股數': 'DL_B(SD)',
        #     '自營商(自行買賣)賣股數': 'DL_S(SD)',
        #     '自營商(自行買賣)淨買股數': 'DL_SD',
        #     '自營商(避險)買股數': 'DL_B(HG)',
        #     '自營商(避險)賣股數': 'DL_S(HG)',
        #     '自營商(避險)淨買股數': 'DL_HG',
        #     '三大法人買賣超股數': 'II',
        # })

        df = df.drop(columns=['名稱名稱', 'Unnamed: 24_level_1Unnamed: 24_level_2'])
        df = df.rename(columns={
            '代號代號': 'SN',
            '外資及陸資(不含外資自營商)買進股數': 'CI_B',
            '外資及陸資(不含外資自營商)賣出股數': 'CI_S',
            '外資及陸資(不含外資自營商)買賣超股數': 'CI',
            '外資自營商買進股數': 'FIDL_B',
            '外資自營商賣出股數': 'FIDL_S',
            '外資自營商買賣超股數': 'FIDL',
            '外資及陸資買進股數': 'FI_B',
            '外資及陸資賣出股數': 'FI_S',
            '外資及陸資買賣超股數': 'FI',
            '投信買進股數': 'IT_B',
            '投信賣出股數': 'IT_S',
            '投信買賣超股數': 'IT',
            '自營商(自行買賣)買進股數': 'DL_B(SD)',
            '自營商(自行買賣)賣出股數': 'DL_S(SD)',
            '自營商(自行買賣)買賣超股數': 'DL_SD',
            '自營商(避險)買進股數': 'DL_B(HG)',
            '自營商(避險)賣出股數': 'DL_S(HG)',
            '自營商(避險)買賣超股數': 'DL_HG',
            '自營商買進股數': 'DL_B',
            '自營商賣出股數': 'DL_S',
            '自營商買賣超股數': 'DL',
            '三大法人買賣超股數合計三大法人買賣超股數合計': 'II',
        })

        df['DATE'] = self.date
        db_data = df.to_dict('record')
        return db_data

    def save(self, content):
        if len(content) == 1:
            return
        self.get_collection(self.table_name).insert_many(content)
