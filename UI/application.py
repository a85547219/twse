import tkinter as tk
from tkinter import ttk
import random
from apscheduler.schedulers.background import BackgroundScheduler
from datetime import datetime, timedelta
from Service.parserService import Parser
from Service.databaseService import Database
from Service.analysisService import Analysis
from Tool.reportTool import PrintReport as pr
from commonCode import *
from ServiceImplement.AnalysisImpl.coinStrategy import CoinStrategy
from ServiceImplement.AnalysisImpl.kdStrategy import KdStrategy
from ServiceImplement.AnalysisImpl.regressionStrategy import RegressionStrategy


class SpiderApplication(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.parser_service = Parser()
        self.analysis_service = Analysis()

        self.all_stocks = Database.get_all_stocks()
        self.date_str = tk.StringVar()
        self.stop_loss_point = tk.IntVar()
        self.stop_loss_point.set(10)
        self.is_show_line = tk.BooleanVar()
        self.is_show_line.set(True)
        self.is_show_win_line = tk.BooleanVar()
        self.is_show_win_line.set(True)
        self.is_show_loss_line = tk.BooleanVar()
        self.is_show_loss_line.set(True)
        self.is_show_fail_line = tk.BooleanVar()
        self.is_show_fail_line.set(False)
        self.is_show_fig = tk.BooleanVar()
        self.is_show_fig.set(False)
        self.is_export = tk.BooleanVar()
        self.is_export.set(False)
        self.is_show_detail = tk.BooleanVar()
        self.is_show_detail.set(True)
        self.is_show_report = tk.BooleanVar()
        self.is_show_report.set(True)
        self.is_show_summary = tk.BooleanVar()
        self.is_show_summary.set(False)
        self.gap_date = tk.StringVar
        self.stock_no = tk.StringVar()
        self.stock_no.set(6257)
        self.master = master
        self.winfo_toplevel().title("Control panel")
        self.grid()
        self.create_widgets()
        self.load_date()

    def get_date(self):
        return datetime.strptime(self.date_str.get(), '%Y%m%d')

    def load_date(self):
        now_time = datetime.now()
        days = 0 if now_time.hour > 13 else 1 if now_time.weekday() is not 0 else 3
        self.lastest_date = now_time - timedelta(days=days)
        self.date_str.set(self.lastest_date.strftime("%Y%m%d"))

    def check_date(self):
        Database.check_latest_count_by_date(self.get_date())

    def create_widgets(self):
        parser_layer = 0
        tk.Button(self, width=8, text='D', command=self.load_date).grid(row=parser_layer, column=0)
        tk.Entry(self, width=8, textvariable=self.date_str).grid(row=parser_layer, column=1)
        tk.Button(self, width=8, text='Daily', command=self.daily_parser).grid(row=parser_layer, column=2)
        tk.Button(self, width=8, text='Week', command=self.week_parser).grid(row=parser_layer, column=3)
        tk.Button(self, width=8, text='Delete', command=self.delete_daily_job).grid(row=parser_layer, column=4)
        tk.Button(self, width=8, text='Check', command=self.check_date).grid(row=parser_layer, column=5)
        tk.Entry(self, width=4, textvariable=self.stop_loss_point).grid(row=parser_layer, column=6)

        op_layer = parser_layer + 1
        tk.Button(self, width=8, text='Random', command=self.get_random_stock).grid(row=op_layer, column=0)
        tk.Entry(self, width=8, textvariable=self.stock_no).grid(row=op_layer, column=1)
        tk.Button(self, width=8, text='All', command=self._all).grid(row=op_layer, column=2)
        tk.Button(self, width=8, text='Run', command=self._run).grid(row=op_layer, column=3)
        tk.Button(self, width=8, text='Figure', command=self._figure).grid(row=op_layer, column=4)

        op_layer2 = op_layer + 1
        tk.Button(self, width=8, text='schedule', command=self._scheduler).grid(row=op_layer2, column=0)
        tk.Button(self, width=8, text='reSend', command=pr.resend_mail).grid(row=op_layer2, column=5)
        tk.Button(self, width=8, text='Mail', command=self._mail).grid(row=op_layer2, column=6)

        option_layer = op_layer2 + 1
        tk.Checkbutton(self, width=4, text='Fig', variable=self.is_show_fig).grid(row=option_layer, column=0)
        tk.Checkbutton(self, width=4, text='Tag', variable=self.is_show_line).grid(row=option_layer, column=1)
        tk.Checkbutton(self, width=4, text='Report.', variable=self.is_show_report).grid(row=option_layer, column=2)
        tk.Checkbutton(self, width=4, text='Detail', variable=self.is_show_detail).grid(row=option_layer, column=3)
        tk.Checkbutton(self, width=4, text='Sum', variable=self.is_show_summary).grid(row=option_layer, column=4)
        tk.Checkbutton(self, width=4, text='Export', variable=self.is_export).grid(row=option_layer, column=5)

        tag_layer = option_layer + 1
        tk.Checkbutton(self, width=4, text='win', variable=self.is_show_win_line).grid(row=tag_layer, column=0)
        tk.Checkbutton(self, width=4, text='loss', variable=self.is_show_loss_line).grid(row=tag_layer, column=1)
        tk.Checkbutton(self, width=4, text='fail', variable=self.is_show_fail_line).grid(row=tag_layer, column=2)

        self.strategyBox = ttk.Combobox(self, width=8, state='readonly')
        self.strategyBox['values'] = ['coin', 'regress', 'kd']
        self.strategyBox.grid(row=tag_layer, column=5)
        self.strategyBox.current(0)
        msg_layer = tag_layer + 1
        self.console = tk.Label(root, text="")
        self.console.grid(row=msg_layer, sticky='we')

    def _scheduler(self):

        def scrape_mm():
            self.console['text'] = 'Fin MM Working'
            self.parser_service.fin_mon_parser(self.get_date())
            self.console['text'] = 'Fin MM Down'

        def scrape_qq():
            self.console['text'] = 'Fin QQ Working'
            self.parser_service.fin_quo_parser(self.get_date())
            self.console['text'] = 'Fin QQ Down'

        def scrape_week():
            self.console['text'] = 'Weekly-ParserImpl Working'
            self.parser_service.week_parser()
            self.console['text'] = 'Weekly-ParserImpl Down'

        def scrape_basic():
            self.load_date()
            self.console['text'] = 'Basic-ParserImpl Working'
            self.parser_service.auto_script(True, self.get_date(), self.lastest_date)
            self.console['text'] = 'Basic-ParserImpl Down'

        def scrape_another():
            self.load_date()
            self.console['text'] = 'ParserImpl Working'
            self.parser_service.auto_script(False, self.get_date(), self.lastest_date)
            self.console['text'] = 'ParserImpl Down'

        def set_console():
            self.console['text'] = "Schedule Keeping... "

        def mail_job():
            self.console['text'] = 'Mail Job Working'
            self._mail()
            self.console['text'] = 'DOWN'

        scheduler = BackgroundScheduler()
        scheduler.add_job(set_console, 'cron', day_of_week='mon-fri', hour=8, minute=00)
        scheduler.add_job(scrape_another, 'cron', day_of_week='mon-fri', hour=11, minute=30)
        scheduler.add_job(scrape_basic, 'cron', day_of_week='mon-fri', hour=16, minute=10)
        scheduler.add_job(mail_job, 'cron', day_of_week='mon-fri', hour=16, minute=20)
        scheduler.add_job(scrape_week, 'cron', day_of_week='mon', hour=10, minute=30)
        scheduler.add_job(scrape_mm, 'cron', day='1-15', hour=16, minute=00)
        scheduler.add_job(scrape_qq, 'cron', month='3,5,8,11', day='10-31', hour=16, minute=00)
        scheduler.add_job(scrape_qq, 'cron', month='4,6,9,12', day='1-5', hour=16, minute=00)
        self.console['text'] = "Schedule Start "
        scheduler.start()

    def get_random_stock(self):
        self.stock_no.set(random.choice(self.all_stocks))

    def get_stocks(self):
        if self.stock_no.get().lower() == 'all':
            return self.all_stocks
        elif self.stock_no.get().lower() == 'set1':
            return ['6257', '6269']
        elif self.stock_no.get().lower() == 'set2':
            return ['3229', '3231', '3094', '3167', '9955']
        return [self.stock_no.get()]

    def daily_parser(self):
        self.parser_service.daily_parser(self.get_date(), self.lastest_date)

    def week_parser(self):
        self.parser_service.week_parser()

    def delete_daily_job(self):
        Database.daily_delete(self.get_date())

    def _figure(self):
        rst = Analysis.do(self.get_stocks(), RunModeEnum.ONE, self.stop_loss_point.get() / 100)
        Analysis.after_run_job(rst, {
            'showDetail': self.is_show_detail.get(),
            'showReport': self.is_show_report.get(),
            'showSummary': True})
        if self.is_show_fig.get():
            Analysis.show_figure(rst[0], {'line': self.is_show_line.get(),
                                          'winLine': self.is_show_win_line.get(),
                                          'lossLine': self.is_show_loss_line.get(),
                                          'failLine': self.is_show_fail_line.get()})

    def _run(self):
        Analysis.console(self.get_stocks(), self._get_strategy())

    def _all(self):
        rst = Analysis.do(self.get_stocks(), RunModeEnum.MANY, self.stop_loss_point.get() / 100,
                          strategy=self._get_strategy())
        Analysis.after_run_job(rst, {
            'showDetail': self.is_show_detail.get(),
            'showReport': self.is_show_report.get(),
            'showSummary': self.is_show_summary.get()})

    def _mail(self):
        Analysis.mail(self.all_stocks, self._get_strategy())

    def _get_strategy(self):
        if self.strategyBox.get() == 'coin':
            return CoinStrategy
        elif self.strategyBox.get() == 'regress':
            return RegressionStrategy
        else:
            return KdStrategy


if __name__ == '__main__':
    root = tk.Tk()
    app = SpiderApplication(master=root)
    app.mainloop()
