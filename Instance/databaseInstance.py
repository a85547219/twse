from Instance.decorator import Singleton
import pymongo
import settings


class Database(Singleton):

    def __init__(self):
        self.client = pymongo.MongoClient(settings.MONGO_HOST, settings.MONGO_POST)
        self._db = self.client[settings.MONGO_DB]

    @property
    def db(self):
        return self._db
