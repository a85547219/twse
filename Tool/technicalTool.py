from commonCode import *


class Trend:
    _status = TrendEnum.SMOOTH_AFTER_UP
    _pre_ma = 0

    def check(self, ma):
        short_trend = (ma - self._pre_ma) / ma * 100
        result = TrendEnum.SMOOTH_AFTER_UP
        if short_trend > 0.18:
            result = TrendEnum.UP_BURST
            self._status = TrendEnum.UP_BURST
        elif short_trend > 0.12:
            result = TrendEnum.UP_NORMAL
            self._status = TrendEnum.UP_NORMAL
        elif short_trend > 0.07:
            result = TrendEnum.UP_WEAK
        elif short_trend < -0.18:
            result = TrendEnum.DOWN_BURST
            self._status = TrendEnum.DOWN_BURST
        elif short_trend < -0.12:
            result = TrendEnum.DOWN_NORMAL
            self._status = TrendEnum.DOWN_NORMAL
        elif short_trend < -0.07:
            result = TrendEnum.DOWN_WEAK
        else:
            if self._status in TrendEnum_UP_S:
                result = TrendEnum.SMOOTH_AFTER_UP
            elif self._status in TrendEnum_DOWN_S:
                result = TrendEnum.SMOOTH_AFTER_DOWN
        self._pre_ma = ma
        return result


class CrossOver:
    _pre_slow = 0
    _pre_fast = 0
    _cross_price = 100
    _pre_result = CrossOverEnum.NO_CROSS

    def check(self, fast_line, slow_line):
        """
        :param slow_line:
        :param fast_line:
        :return: G,D,N
        """
        result = self._pre_result
        if self._pre_fast < self._pre_slow and fast_line > slow_line:
            result = CrossOverEnum.GOLD_CROSS
            self._cross_price = (self._pre_fast + fast_line) / 2
            self._pre_result = CrossOverEnum.AFTER_G
        elif self._pre_slow < self._pre_fast and slow_line > fast_line:
            result = CrossOverEnum.DEAD_CROSS
            self._cross_price = (self._pre_fast + fast_line) / 2
            self._pre_result = CrossOverEnum.AFTER_D

        trend = CrossOverEnum.FAST_UP if self._pre_fast < fast_line else CrossOverEnum.FAST_DOWN
        self._pre_fast = fast_line
        self._pre_slow = slow_line
        return result, self._cross_price, trend



