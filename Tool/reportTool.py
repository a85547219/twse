import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from commonCode import *
from settings import *
from ServiceImplement.DatabaseImpl.emailRecordRepo import EmailRecordRepo
from ServiceImplement.DatabaseImpl.stockNoRepository import StockNoTable
from datetime import datetime

from Tool.tradingTool import CommonTool


class PrintReport:

    @staticmethod
    def strategy_comment_to_html(comment_list):
        rst = ""
        for comment in comment_list:
            rst += "<br />" + comment
        return rst

    @staticmethod
    def summary_to_html(summary):
        def table_format(*args):
            tr_begin = '<tr style="height: 17px;">'
            td = '<td><span style="color: {};">{}</span></td>'
            tr_end = '</tr>'
            rst = tr_begin
            mcm, buy, sell, diff = args
            rst += td.format('#fffff', mcm)
            rst += td.format('#fffff', CommonTool.amount_to_string(buy))
            rst += td.format('#fffff', CommonTool.amount_to_string(sell))
            rst += td.format('#04B431' if diff < 0 else '#ff0000', CommonTool.amount_to_string(diff))
            rst += tr_end

            return rst

        """
            大盤結算資料。
        :param data: ii_sum
        {'MCM': '自營商(自行買賣)', 'B_A': 1747775460, 'S_A': 1224038791, 'B&S_A': 523736669}
        {'MCM': '自營商(避險)', 'B_A': 6689806046, 'S_A': 7204306182, 'B&S_A': -514500136}
        {'MCM': '投信', 'B_A': 1708535702, 'S_A': 2463736362, 'B&S_A': -755200660}
        {'MCM': '外資及陸資', 'B_A': 37440234734, 'S_A': 32231321807, 'B&S_A': 5208912927}
        {'MCM': '合計', 'B_A': 47586351942, 'S_A': 43123403142, 'B&S_A': 4462948800}
        """
        table_begin = '<table style="width: 100%; max-width: 500px;"><tbody>'
        table_row = '''
            <tr style="height: 17px;">
            <td style="width: 31%; height: 17px;">單位名稱</td>
            <td style="width: 23%; height: 17px;">買進金額(億)</td>
            <td style="width: 23%; height: 17px;">賣出金額(億)</td>
            <td style="width: 23%; height: 17px;">買賣差額(億)</td>
            </tr>
        '''
        if not summary:
            return ""
        for data in summary:
            table_row += table_format(data['MCM'], data['B_A'], data['S_A'], data['B&S_A'])

        table_end = '</tbody></table>'
        split_line = '<hr style="border: 1px solid; border-collapse: collapse; width: 100%;max-width: 500px;" />'
        html = table_begin + table_row + table_end
        return html

    @staticmethod
    def comment_to_html(date, count):
        """
            補充資料。
        """
        split_line = '<p>&nbsp;</p><hr border="1" cellpadding="3" cellspacing="1" style="border: 1px solid ; border-collapse: collapse; width:100%;max-width: 500px;" align="left"/>'
        basic_info = "個股資料最後日期: {}。<br />共 {} 檔個股。".format(date.strftime("%Y%m%d"), count)
        return split_line + basic_info

    @staticmethod
    def daily_data_to_html(order):
        """
            個股資料。
        :param order: 每日盤後資料
        """

        def _split_list(lists):
            if lists:
                _concat_rst = ""
                for i in range(1, len(lists)):
                    _concat_rst += "," + lists[i]
                return lists[0] + _concat_rst
            else:
                return ""

        def _format_msg(_enum):
            header = '<p style="text-align: center;max-width: 500px;"><strong> {} </strong></p>'.format(
                _enum.value + "-" + str(msg_dict_count[_enum]) + "檔")
            container_of_body = '<table border="1" width="100%" style="max-width: 500px;" cellspacing="0" cellpadding="3"><tbody> {} </tbody></table>'
            container_of_body_template = '<tr> {} {} {}</tr><tr> {} </tr>'
            body = ''
            stock_and_name = '<td style="width: 25%; text-align: left;">&nbsp;{}</td>'
            category = '<td style="width: 25%;">&nbsp;{}</td>'
            reason = '<td style="width: 50%;">&nbsp;{}</td>'
            detail = '<td style="width: 100%;" colspan="3">&nbsp;{}</td>'
            for msg in msg_dict[_enum]:
                body += container_of_body_template.format(
                    stock_and_name.format(msg['stock_and_name']), category.format(msg['category']),
                    reason.format(msg['reason']), detail.format(msg['detail']))
            return header + container_of_body.format(body) if body else ""

        stock_maps = StockNoTable().get_map()
        msg_dict = {TradeAction.NOTICE_BUY: [], TradeAction.BUY: [], TradeAction.MUST_BUY: [],
                    TradeAction.NOTICE_SELL: [], TradeAction.SELL: [], TradeAction.MUST_SELL: []}
        msg_dict_count = {TradeAction.NOTICE_BUY: 0, TradeAction.BUY: 0, TradeAction.MUST_BUY: 0,
                          TradeAction.NOTICE_SELL: 0, TradeAction.SELL: 0, TradeAction.MUST_SELL: 0}
        for _sn, _act, _price, _reason in order:
            if _act in [None, TradeAction.DO_NOTHING]:
                continue
            _p = _price if _price is not None else ""
            _r, _d = _split_list(_reason[0]), _split_list(_reason[1])

            stock_map = stock_maps.get(_sn, {'NAME': "", 'CATEGORY': ""})
            msg_dict[_act].append({'stock_and_name': _sn + stock_map.get('NAME', "unknown"),
                                   'category': stock_map.get('CATEGORY', "unknown"),
                                   'reason': _r,
                                   'detail': _d})
            msg_dict_count[_act] += 1
        return _format_msg(TradeAction.BUY) + _format_msg(TradeAction.MUST_BUY)

    @staticmethod
    def daily_data_to_print(order):
        """
            個股資料。
        :param order: 每日盤後資料
        """

        def _print_msg(_enum):
            for msg in msg_dict[_enum]:
                print(msg['stock_and_name'] + " - " + msg['category'])
                print(msg['reason'] + msg['detail'])
                print("====================")

            if msg_dict_count[_enum] != 0: print(_enum.value + "-" + str(msg_dict_count[_enum]))

        stock_maps = StockNoTable().get_map()
        msg_dict = {TradeAction.NOTICE_BUY: [], TradeAction.BUY: [], TradeAction.MUST_BUY: [],
                    TradeAction.NOTICE_SELL: [], TradeAction.SELL: [], TradeAction.MUST_SELL: []}
        msg_dict_count = {TradeAction.NOTICE_BUY: 0, TradeAction.BUY: 0, TradeAction.MUST_BUY: 0,
                          TradeAction.NOTICE_SELL: 0, TradeAction.SELL: 0, TradeAction.MUST_SELL: 0}
        for _sn, _act, _price, _reason in order:
            if _act in [TradeAction.BUY, TradeAction.MUST_BUY]:
                stock_map = stock_maps.get(_sn, {'NAME': "", 'CATEGORY': ""})
                msg_dict[_act].append({'stock_and_name': _sn + stock_map['NAME'],
                                       'category': stock_map['CATEGORY'],
                                       'reason': _reason[0],
                                       'detail': _reason[1]})
                msg_dict_count[_act] += 1
        _print_msg(TradeAction.BUY)
        _print_msg(TradeAction.MUST_BUY)

    @staticmethod
    def record_mail(title, *args):
        content = ""
        for arg in args:
            content += arg
        EmailRecordRepo().save_record(EMAIL_DESTINATION, title, content, datetime.now())

    @staticmethod
    def resend_mail():
        doc = EmailRecordRepo().get_lasted_data()
        PrintReport.send_mail(doc['subject'], doc['content'])

    @staticmethod
    def send_mail(title, *args):
        """
        :param title: 信件標題
        :param args:  MIMEText
        """
        PrintReport.record_mail(title, *args)
        msg = MIMEMultipart()
        for arg in args:
            msg.attach(MIMEText(arg, 'html'))
        msg['Subject'] = title
        msg['From'] = GMAIL_USER
        msg['To'] = EMAIL_DESTINATION
        try:
            server = smtplib.SMTP_SSL(SMTP_IP, SMTP_PORT)
            server.ehlo()
            server.login(GMAIL_USER, GMAIL_PASSWORD)
            server.send_message(msg)
            server.quit()
            print('Email sent')
        except smtplib.SMTPException:
            print('Email sent fail')

    @staticmethod
    def print_detail(win, loss, keep):
        def detail(doc, price, end='\n'):
            print('D %s kd %.2f %.2f v %.d map %.3f p %.2f ' % (
                doc[IndexEnum.DATE].strftime("%m/%d/%Y"),
                doc[IndexEnum.K], doc[IndexEnum.D], doc[IndexEnum.VOL],
                doc[IndexEnum.MAP] * 100, price
            ), end=end)

        def detail_template(doc_collection):
            for buy_list, (sell_detial, sell_price), (date, current_max) in doc_collection:
                buy_collection = []
                first_date = buy_list[0][0][IndexEnum.DATE]
                for buy_detail, buy_price in buy_list:
                    buy_collection.append(buy_price)
                    detail(buy_detail, buy_price)
                detail(sell_detial, sell_price, end='')
                average_buy = sum(buy_collection) / len(buy_collection)
                print(' R %.2f D %d BP %s %.2f' % (
                    (sell_price - average_buy) / average_buy * 100, (sell_detial[IndexEnum.DATE] - first_date).days,
                    date.strftime("%m/%d"),
                    current_max))

        def keep_detail():
            if len(keep) == 1:
                return
            for buy_detail, buy_price in keep[:-1]:
                detail(buy_detail, buy_price)
            print("BP %s %.2f" % (keep[-1][0].strftime("%m/%d"), keep[-1][1]))

        detail_template(loss)
        print('---------------Win------------------')
        detail_template(win)
        # in hand
        keep_detail()

    @staticmethod
    def print_order(order, is_export=False):
        tmp = "Action {}, Price {}, Reason: {}\n".format(order[1], order[2], order[3])
        print(tmp)
        if is_export:
            PrintReport.export('Report.txt', tmp)

    @staticmethod
    def print_report(report, is_export=False):
        tmp = "SN-{}\n W {}, R {}%, R ACC {}%, MAX {}%, MIN {}%\n Trade {}, Buy {}, Win {}, Lose {}\n Buy Fail {}, Sell Fail {}, Stop {}\n K {}, Long Keep {}, Short Keep {} ".format(
            report[ReportEnum.STOCK_NO], report[ReportEnum.WIN_RATE], report[ReportEnum.RETURN_AVG],
            report[ReportEnum.RETURN_ACC], report[ReportEnum.RETURN_MAX], report[ReportEnum.RETURN_MIN],
            report[ReportEnum.TRADE_TIMES], report[ReportEnum.BUY_TIMES], report[ReportEnum.WIN_TIMES],
            report[ReportEnum.LOSE_TIMES], report[ReportEnum.BUY_FAIL], report[ReportEnum.SELL_FAIL],
            report[ReportEnum.STOP], report[ReportEnum.KEEP_DAY_AVG], report[ReportEnum.KEEP_DAY_LONG],
            report[ReportEnum.KEEP_DAY_SHORT])
        print(tmp)
        if is_export:
            PrintReport.export('Report.txt', tmp)

    @staticmethod
    def print_summary(report_data, date_collection, is_export=False):
        doc = PrintReport._summary_data(report_data)
        st = "===================Summary=====================\n" + "All {}, Trade {}, No Trade {}\n".format(
            doc[SummaryReportEnum.TOTAL],
            doc[SummaryReportEnum.TOTAL_WITH_TRADE],
            doc[SummaryReportEnum.TOTAL_WITHOUT_TRADE]) + "W(%) {}, Max(%) {}, Min(%) {}\n".format(
            doc[SummaryReportEnum.WIN_RATE],
            doc[SummaryReportEnum.WIN_RATE_MAX],
            doc[SummaryReportEnum.WIN_RATE_MIN]) + "平均投報率: AVG {}, ACC {}, Max {}, Min {}\n".format(
            round(doc[SummaryReportEnum.RETURN_AVG], 2),
            round(doc[SummaryReportEnum.RETURN_AVG_ACC], 2),
            doc[SummaryReportEnum.RETURN_AVG_MAX],
            doc[SummaryReportEnum.RETURN_AVG_MIN]) + "累積投報率: AVG {}, ACC {}, Max {}, Min {}\n".format(
            doc[SummaryReportEnum.RETURN_ACC_AVG],
            doc[SummaryReportEnum.RETURN_ACC],
            doc[SummaryReportEnum.RETURN_ACC_MAX],
            doc[SummaryReportEnum.RETURN_ACC_MIN]) + "One Trade Max Return(%) {}, Avg {}, Min {}, Avg {}\n".format(
            doc[SummaryReportEnum.RETURN_MAX_ONE_TRADE],
            doc[SummaryReportEnum.RETURN_MAX_ONE_TRADE_AVG],
            doc[SummaryReportEnum.RETURN_MIN_ONE_TRADE],
            doc[SummaryReportEnum.RETURN_MIN_ONE_TRADE_AVG]) + "Trade {}, T Avg {}, Buy {}, B Avg {}\n".format(
            doc[SummaryReportEnum.TRADE_TIMES],
            doc[SummaryReportEnum.TRADE_TIMES_AVG],
            doc[SummaryReportEnum.BUY_TIMES],
            doc[SummaryReportEnum.BUY_TIMES_AVG]) + " Win {}, W Avg {}, Lose {}, L Avg {}\n".format(
            doc[SummaryReportEnum.WIN_TIMES],
            doc[SummaryReportEnum.WIN_TIMES_AVG],
            doc[SummaryReportEnum.LOSE_TIMES],
            doc[
                SummaryReportEnum.LOSE_TIMES_AVG]) + "Buy Fail {}, BF Avg {}, Sell Fail {}, SF Avg {}, Stop {}, S Max {}, S Avg {}\n".format(
            doc[SummaryReportEnum.BUY_FAIL_TIMES],
            doc[SummaryReportEnum.BUY_FAIL_TIMES_AVG],
            doc[SummaryReportEnum.SELL_FAIL_TIMES],
            doc[SummaryReportEnum.SELL_FAIL_TIMES_AVG],
            doc[SummaryReportEnum.STOP_TIMES],
            doc[SummaryReportEnum.STOP_TIMES_MAX],
            doc[
                SummaryReportEnum.STOP_TIMES_AVG]) + "K {}, Long K {}, LK Avg. {}, Short K {}, SK Avg. {}, Date Times {}".format(
            doc[SummaryReportEnum.KEEP_DAY_AVG],
            doc[SummaryReportEnum.KEEP_DAY_LONG],
            doc[
                SummaryReportEnum.KEEP_DAY_LONG_AVG],
            doc[SummaryReportEnum.KEEP_DAY_SHORT],
            doc[
                SummaryReportEnum.KEEP_DAY_SHORT_AVG],
            len(date_collection))
        print(st)

        def _no_trade_detail():
            tmp = ""
            tmp += "{}".format(doc[SummaryReportEnum.NO_TRADE_COLLECTION]) if doc[
                SummaryReportEnum.TOTAL_WITHOUT_TRADE] else ""
            if tmp:
                print("No Trade Detail")
                print(tmp)

        _no_trade_detail()
        if is_export:
            PrintReport.export('Report.txt', st)

    @staticmethod
    def export(title, msg):
        f = open(title, "a+")
        f.write(msg)
        f.close()

    @staticmethod
    def _summary_data(report_data):
        def extract_zero_trade():
            # 抽出沒有任何交易的個股。
            _rst, _tmp = [], []
            for _doc in report_data:
                if _doc[ReportEnum.TRADE_TIMES] == 0:
                    _rst.append(_doc[ReportEnum.STOCK_NO])
                    _tmp.append(_doc)
            for _doc in _tmp:
                report_data.remove(_doc)
            return _rst

        def max_min_avg_total(enum):
            _max, _minn = 0, 999
            _max_stock, _min_stock = '', ''
            _count = 0
            for _dict_doc in report_data:
                _count += _dict_doc[enum]
                if _dict_doc[enum] > _max:
                    _max = _dict_doc[enum]
                    _max_stock = _dict_doc[ReportEnum.STOCK_NO]
                if _dict_doc[enum] < _minn:
                    _minn = _dict_doc[enum]
                    _min_stock = _dict_doc[ReportEnum.STOCK_NO]
            return _max, _max_stock, _minn, _min_stock, round(
                _count / (1 if len(report_data) == 0 else len(report_data)),
                3), _count

        summary_dict = {SummaryReportEnum.TOTAL: len(report_data)}
        _noTradeCollection = extract_zero_trade()
        summary_dict[SummaryReportEnum.TOTAL_WITH_TRADE] = len(report_data)
        summary_dict[SummaryReportEnum.TOTAL_WITHOUT_TRADE] = len(_noTradeCollection)
        summary_dict[SummaryReportEnum.NO_TRADE_COLLECTION] = _noTradeCollection
        _mx, _mxn, _mi, _min, _a, _c = max_min_avg_total(ReportEnum.WIN_RATE)
        summary_dict[SummaryReportEnum.WIN_RATE] = _a
        summary_dict[SummaryReportEnum.WIN_RATE_MAX] = "{}({})".format(_mx, _mxn)
        summary_dict[SummaryReportEnum.WIN_RATE_MIN] = "{}({})".format(_mi, _min)
        _mx, _mxn, _mi, _min, _a, _c = max_min_avg_total(ReportEnum.RETURN_ACC)
        summary_dict[SummaryReportEnum.RETURN_ACC] = _c
        summary_dict[SummaryReportEnum.RETURN_ACC_AVG] = _a
        summary_dict[SummaryReportEnum.RETURN_ACC_MAX] = "{}({})".format(_mx, _mxn)
        summary_dict[SummaryReportEnum.RETURN_ACC_MIN] = "{}({})".format(_mi, _min)
        _mx, _mxn, _mi, _min, _a, _c = max_min_avg_total(ReportEnum.RETURN_AVG)
        summary_dict[SummaryReportEnum.RETURN_AVG] = _a
        summary_dict[SummaryReportEnum.RETURN_AVG_ACC] = _c
        summary_dict[SummaryReportEnum.RETURN_AVG_MAX] = "{}({})".format(_mx, _mxn)
        summary_dict[SummaryReportEnum.RETURN_AVG_MIN] = "{}({})".format(_mi, _min)
        _mx, _mxn, _mi, _min, _a, _c = max_min_avg_total(ReportEnum.RETURN_MAX)
        summary_dict[SummaryReportEnum.RETURN_MAX_ONE_TRADE_AVG] = _a
        summary_dict[SummaryReportEnum.RETURN_MAX_ONE_TRADE] = "{}({})".format(_mx, _mxn)
        _mx, _mxn, _mi, _min, _a, _c = max_min_avg_total(ReportEnum.RETURN_MIN)
        summary_dict[SummaryReportEnum.RETURN_MIN_ONE_TRADE_AVG] = _a
        summary_dict[SummaryReportEnum.RETURN_MIN_ONE_TRADE] = "{}({})".format(_mi, _min)
        _mx, _mxn, _mi, _min, _a, _c = max_min_avg_total(ReportEnum.TRADE_TIMES)
        summary_dict[SummaryReportEnum.TRADE_TIMES] = _c
        summary_dict[SummaryReportEnum.TRADE_TIMES_AVG] = _a
        _mx, _mxn, _mi, _min, _a, _c = max_min_avg_total(ReportEnum.BUY_TIMES)
        summary_dict[SummaryReportEnum.BUY_TIMES] = _c
        summary_dict[SummaryReportEnum.BUY_TIMES_AVG] = _a
        _mx, _mxn, _mi, _min, _a, _c = max_min_avg_total(ReportEnum.WIN_TIMES)
        summary_dict[SummaryReportEnum.WIN_TIMES] = _c
        summary_dict[SummaryReportEnum.WIN_TIMES_AVG] = _a
        _mx, _mxn, _mi, _min, _a, _c = max_min_avg_total(ReportEnum.LOSE_TIMES)
        summary_dict[SummaryReportEnum.LOSE_TIMES] = _c
        summary_dict[SummaryReportEnum.LOSE_TIMES_AVG] = _a
        _mx, _mxn, _mi, _min, _a, _c = max_min_avg_total(ReportEnum.SELL_FAIL)
        summary_dict[SummaryReportEnum.SELL_FAIL_TIMES] = _c
        summary_dict[SummaryReportEnum.SELL_FAIL_TIMES_AVG] = _a
        _mx, _mxn, _mi, _min, _a, _c = max_min_avg_total(ReportEnum.BUY_FAIL)
        summary_dict[SummaryReportEnum.BUY_FAIL_TIMES] = _c
        summary_dict[SummaryReportEnum.BUY_FAIL_TIMES_AVG] = _a
        _mx, _mxn, _mi, _min, _a, _c = max_min_avg_total(ReportEnum.STOP)
        summary_dict[SummaryReportEnum.STOP_TIMES] = _c
        summary_dict[SummaryReportEnum.STOP_TIMES_MAX] = "{}({})".format(_mx, _mxn)
        summary_dict[SummaryReportEnum.STOP_TIMES_AVG] = _a
        _mx, _mxn, _mi, _min, _a, _c = max_min_avg_total(ReportEnum.KEEP_DAY_AVG)
        summary_dict[SummaryReportEnum.KEEP_DAY_AVG] = _a
        _mx, _mxn, _mi, _min, _a, _c = max_min_avg_total(ReportEnum.KEEP_DAY_LONG)
        summary_dict[SummaryReportEnum.KEEP_DAY_LONG] = "{}({})".format(_mx, _mxn)
        summary_dict[SummaryReportEnum.KEEP_DAY_LONG_AVG] = _a
        _mx, _mxn, _mi, _min, _a, _c = max_min_avg_total(ReportEnum.KEEP_DAY_SHORT)
        summary_dict[SummaryReportEnum.KEEP_DAY_SHORT] = "{}({})".format(_mi, _min)
        summary_dict[SummaryReportEnum.KEEP_DAY_SHORT_AVG] = _a

        return summary_dict
