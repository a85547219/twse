import matplotlib.pyplot as plt
import mplfinance.original_flavor as mpf
import numpy as np
import tkinter as tk
from matplotlib.backends.backend_tkagg import (FigureCanvasTkAgg, NavigationToolbar2Tk)
from commonCode import IndexEnum, LineColorEnum, TechEnum


class Fig:

    def __init__(self, map_doc, all_doc):
        self._show_line = True
        self._show_loss = True
        self._show_win = True
        self._show_fail = True
        self._title_name = 'stock analysis fig'
        self.all_doc = all_doc
        self.map_doc = map_doc
        self.fig = plt.figure(figsize=(12, 7))
        plt.subplots_adjust(top=1, bottom=0.05, left=0.05, right=0.9, hspace=0.1)
        self._main_plot()
        self.main_ax.callbacks.connect('xlim_changed', self._rescale_y)

    def set_title_name(self, str_title):
        self._title_name = str_title

    def set_fig_config(self, tag=True, win=True, loss=True, fail=True):
        self._show_line = tag
        self._show_win = win
        self._show_loss = loss
        self._show_fail = fail

    def run(self, win_buy, win_sell, loss_buy, loss_sell, keep, stop, fail_buy, fail_sell):
        self._kd_plot()
        self._macd_plot()
        self._dmi_plot()
        self._rsi_plot()
        self._vol_plot()
        if self._show_line:
            if self._show_win:
                self._trade_plot(win_buy[0], linestyle='-', color=LineColorEnum.BUY_TAG.value)
                self._trade_plot(win_sell[0], color=LineColorEnum.SELL_WIN.value)
            if self._show_loss:
                self._trade_plot(loss_buy[0], linestyle='-', color=LineColorEnum.BUY_TAG.value)
                self._trade_plot(loss_sell[0], color=LineColorEnum.SELL_LOSS.value)
            self._trade_plot(keep[0], linestyle='-', color=LineColorEnum.BUY_TAG.value)
            self._trade_plot(stop[0], color=LineColorEnum.STOP_LOSE.value)
            if self._show_fail:
                self._trade_plot(fail_buy[0], color=LineColorEnum.FAIL_BUY.value)
                self._trade_plot(fail_sell[0], color=LineColorEnum.FAIL_SELL.value)

        self.buy_map = dict(zip(win_buy[0] + loss_buy[0] + keep[0], win_buy[1] + loss_buy[1] + keep[1]))
        self.sell_win_map = dict(zip(win_sell[0], win_sell[1]))
        self.sell_loss_map = dict(zip(loss_sell[0], loss_sell[1]))
        self.stop_map = dict(zip(stop[0], stop[1]))
        self.buy_fail_map = dict(zip(fail_buy[0], fail_buy[1]))
        self.sell_fail_map = dict(zip(fail_sell[0], fail_sell[1]))
        self._embedding_tk()

    def _trade_plot(self, date, linestyle='solid', color=LineColorEnum.BUY_TAG.value):
        if not date:
            return {}
        date_doc = self.all_doc[IndexEnum.DATE]
        x_location = [np.where(date_doc == doc) for doc in date]
        for x in x_location:
            for ax in self.fig.axes:
                ax.axvline(x=x, linestyle=linestyle, color=color)

    def _main_plot(self):
        self.main_ax = plt.subplot2grid((15, 1), (0, 0), rowspan=5, colspan=1)
        (lines, polys) = mpf.candlestick2_ochl(self.main_ax, self.all_doc[IndexEnum.OPEN],
                                               self.all_doc[IndexEnum.CLOSE], self.all_doc[IndexEnum.HIGH],
                                               self.all_doc[IndexEnum.LOW], width=0.6, colorup='r', colordown='g',
                                               alpha=0.75)
        polys.set_picker(True)
        plt.rcParams['font.sans-serif'] = ['Microsoft JhengHei']
        plt.rcParams['axes.unicode_minus'] = False
        self.main_ax.plot(self.all_doc[IndexEnum.MA10], label='10-ma', color=LineColorEnum.PLUS_FAST.value)
        self.main_ax.plot(self.all_doc[IndexEnum.MA18], label='18-ma', color=LineColorEnum.MINUS_SLOW.value)
        self.main_ax.plot(self.all_doc[IndexEnum.MA30], label='30-ma', color=LineColorEnum.OTHER.value)

        self.main_ax.plot(self.all_doc[IndexEnum.UPPERBAND2], linestyle=':', color=LineColorEnum.BBAND2.value)
        self.main_ax.plot(self.all_doc[IndexEnum.MIDDLEBAND], linestyle='-.', color=LineColorEnum.BLACK.value)
        self.main_ax.plot(self.all_doc[IndexEnum.LOWERBAND2], linestyle=':', color=LineColorEnum.BBAND2.value)
        self.main_ax.plot(self.all_doc[IndexEnum.LOWERBAND1], linestyle=':', color=LineColorEnum.BBAND1.value)
        self.main_ax.plot(self.all_doc[IndexEnum.UPPERBAND1], linestyle=':', color=LineColorEnum.BBAND1.value)
        self.main_ax.legend(bbox_to_anchor=(1.11, 1))
        self.main_ax.yaxis.grid(True)
        self.main_ax.axes.get_xaxis().set_visible(False)

    def _kd_plot(self):
        self.kd_ax = plt.subplot2grid((15, 1), (5, 0), rowspan=2, colspan=1, sharex=self.main_ax)
        self.kd_ax.set_ylabel('kd')
        self.kd_ax.plot(self.all_doc[IndexEnum.K], label='K', color=LineColorEnum.PLUS_FAST.value)
        self.kd_ax.plot(self.all_doc[IndexEnum.D], label='D', color=LineColorEnum.MINUS_SLOW.value)
        self.kd_ax.axes.get_xaxis().set_visible(False)
        self.kd_ax.legend(bbox_to_anchor=(1, 1))
        self.kd_ax.yaxis.grid(True)

    def _macd_plot(self):
        self.macd_ax = plt.subplot2grid((15, 1), (7, 0), rowspan=2, colspan=1, sharex=self.main_ax)
        self.macd_ax.set_ylabel('macd')
        x = np.arange(len(self.all_doc[IndexEnum.HIST]))
        macd = np.nan_to_num(self.all_doc[IndexEnum.HIST], True)
        mask1 = macd >= 0
        mask2 = macd <= 0
        self.macd_ax.bar(x[mask1], self.all_doc[IndexEnum.HIST][mask1], color='r')
        self.macd_ax.bar(x[mask2], self.all_doc[IndexEnum.HIST][mask2], color='g')
        self.macd_ax.plot(self.all_doc[IndexEnum.MACD], label='macd', color=LineColorEnum.MINUS_SLOW.value)
        self.macd_ax.plot(self.all_doc[IndexEnum.DIFF], label='diff', color=LineColorEnum.PLUS_FAST.value)
        self.macd_ax.legend(bbox_to_anchor=(1.1, 1))
        self.macd_ax.axes.get_xaxis().set_visible(False)
        self.macd_ax.yaxis.grid(True)

    def _dmi_plot(self):
        self.dmi_ax = plt.subplot2grid((15, 1), (9, 0), rowspan=2, colspan=1, sharex=self.main_ax)
        self.dmi_ax.set_ylabel('dmi')
        self.dmi_ax.plot(self.all_doc[IndexEnum.PLUS_DI], label='+DI', color=LineColorEnum.PLUS_FAST.value)
        self.dmi_ax.plot(self.all_doc[IndexEnum.MINUS_DI], label='-DI', color=LineColorEnum.MINUS_SLOW.value)
        self.dmi_ax.plot(self.all_doc[IndexEnum.ADX], label='ADX', color=LineColorEnum.OTHER.value)
        self.dmi_ax.legend(bbox_to_anchor=(1.1, 1))
        self.dmi_ax.yaxis.grid(True)
        self.dmi_ax.axes.get_xaxis().set_visible(False)

    def _rsi_plot(self):
        self.rsi_ax = plt.subplot2grid((15, 1), (11, 0), rowspan=2, colspan=1, sharex=self.main_ax)
        self.rsi_ax.set_ylabel('rsi')
        self.rsi_ax.plot(self.all_doc[IndexEnum.RSI5], label='rsi-5', color=LineColorEnum.PLUS_FAST.value)
        self.rsi_ax.plot(self.all_doc[IndexEnum.RSI10], label='rsi-10', color=LineColorEnum.MINUS_SLOW.value)
        self.rsi_ax.legend(bbox_to_anchor=(1.1, 1))
        self.rsi_ax.yaxis.grid(True)
        self.rsi_ax.axes.get_xaxis().set_visible(False)

    def _vol_plot(self):
        self.vol_ax = plt.subplot2grid((15, 1), (13, 0), rowspan=2, colspan=1, sharex=self.main_ax)
        self.vol_ax.set_ylabel('vol')
        mpf.volume_overlay(self.vol_ax, self.all_doc[IndexEnum.OPEN], self.all_doc[IndexEnum.CLOSE],
                           self.all_doc[IndexEnum.VOL],
                           colorup='r', colordown='g',
                           width=0.5, alpha=0.8)
        dd = [x.strftime('%Y-%m-%d') for x in self.all_doc[IndexEnum.DATE]]
        self.vol_ax.set_xticks(range(0, len(self.all_doc[IndexEnum.DATE]), 10))
        self.vol_ax.set_xticklabels(dd[::10])
        self.vol_ax.yaxis.grid(True)

    def _embedding_tk(self):
        def _component_place():
            self.l_base = tk.Label(master=right_fm)
            self.l_base.pack(side=tk.TOP)

            self.l_cross = tk.Label(master=right_fm)
            self.l_cross.pack(side=tk.TOP)

            self.l_ma = tk.Label(master=right_fm)
            self.l_ma.pack(side=tk.TOP)

            self.l_kd = tk.Label(master=right_fm)
            self.l_kd.pack(side=tk.TOP)
            self.l_rsi = tk.Label(master=right_fm)
            self.l_rsi.pack(side=tk.TOP)
            self.l_bband1 = tk.Label(master=right_fm)
            self.l_bband1.pack(side=tk.TOP)
            self.l_bband2 = tk.Label(master=right_fm)
            self.l_bband2.pack(side=tk.TOP)
            self.l_macd = tk.Label(master=right_fm)
            self.l_macd.pack(side=tk.TOP)
            self.l_dmi = tk.Label(master=right_fm)
            self.l_dmi.pack(side=tk.TOP)

            self.l_trade = tk.Label(master=right_fm)
            self.l_trade.pack(side=tk.TOP)

        root = tk.Tk()
        root.title(self._title_name)
        left_fm = tk.Frame(root)
        right_fm = tk.Frame(root)
        left_fm.pack(side=tk.LEFT, fill=tk.BOTH, expand=tk.YES)
        right_fm.pack(side=tk.RIGHT, fill=tk.BOTH, expand=tk.YES)
        canvas = FigureCanvasTkAgg(self.fig, master=left_fm)  # A tk.DrawingArea.
        canvas.get_tk_widget().pack(side=tk.BOTTOM, fill=tk.BOTH, expand=1)
        toolbar = NavigationToolbar2Tk(canvas, left_fm)
        canvas.mpl_connect('button_press_event', self._onpick)
        canvas.draw()
        toolbar.update()
        _component_place()
        tk.mainloop()

    def _rescale_y(self, evt=None):
        def get_ybound(max_list, min_list):
            y_max = np.nanmax(max_list)
            y_min = np.nanmin(min_list)
            margin = (y_max - y_min) * 0.05
            return [y_min - margin, y_max + margin]

        xmin, xmax = self.main_ax.get_xlim()
        xmin = int(xmin) if xmin > 0 else 0
        xmax = int(xmax) if xmax < len(self.all_doc[IndexEnum.HIGH]) else len(self.all_doc[IndexEnum.HIGH])
        self.ybounds = get_ybound(self.all_doc[IndexEnum.HIGH][xmin: xmax], self.all_doc[IndexEnum.LOW][xmin: xmax])

        macd_array = np.concatenate((self.all_doc[IndexEnum.DIFF][xmin: xmax], self.all_doc[IndexEnum.MACD][xmin: xmax],
                                     self.all_doc[IndexEnum.HIST][xmin: xmax]))
        self.ybounds_macd = get_ybound(macd_array, macd_array)

        vol_array = self.all_doc[IndexEnum.VOL][xmin: xmax]
        self.ybounds_vol = get_ybound(vol_array, vol_array)

        dmi_array = np.concatenate((self.all_doc[IndexEnum.ADX][xmin:xmax], self.all_doc[IndexEnum.PLUS_DI][xmin:xmax],
                                    self.all_doc[IndexEnum.MINUS_DI][xmin:xmax]))
        self.ybounds_dmi = get_ybound(dmi_array, dmi_array)

        kd_array = np.concatenate((self.all_doc[IndexEnum.K][xmin:xmax], self.all_doc[IndexEnum.D][xmin:xmax]))
        self.ybounds_kd = get_ybound(kd_array, kd_array)

        rsi_array = np.concatenate((self.all_doc[IndexEnum.RSI5][xmin:xmax], self.all_doc[IndexEnum.RSI10][xmin:xmax]))
        self.ybounds_rsi = get_ybound(rsi_array, rsi_array)
        timer = self.main_ax.figure.canvas.new_timer(interval=10)
        timer.single_shot = True
        timer.add_callback(self._change_y)
        timer.start()

    def _change_y(self):
        self.main_ax.set_ylim(self.ybounds)
        self.macd_ax.set_ylim(self.ybounds_macd)
        self.vol_ax.set_ylim(self.ybounds_vol)
        self.dmi_ax.set_ylim(self.ybounds_dmi)
        self.kd_ax.set_ylim(self.ybounds_kd)
        self.rsi_ax.set_ylim(self.ybounds_rsi)
        self.main_ax.figure.canvas.draw()

    axv_line = False

    def _onpick(self, event):
        if not event.xdata or event.xdata < 0 or event.xdata > len(self.map_doc):
            return
        if event.button == 3:
            for ax in self.fig.axes:
                if self.axv_line:
                    ax.lines[-1].remove()
                ax.axvline(x=int(round(event.xdata)), color=LineColorEnum.TAG_LINE.value)
            self.axv_line = True
            self.main_ax.figure.canvas.draw()

        doc = self.map_doc[int(round(event.xdata))]
        self.l_base['text'] = 'd {} {}\noc {} {}\nhl {} {}\nv {} b {}\nap {} map {}'.format(
            doc[IndexEnum.DATE].strftime('%Y%m%d'), int(round(event.xdata)),
            doc[IndexEnum.OPEN], doc[IndexEnum.CLOSE],
            doc[IndexEnum.HIGH], doc[IndexEnum.LOW],
            doc[IndexEnum.VOL], round(doc[IndexEnum.BIAS], 3),
            round(doc[IndexEnum.AP] * 100, 3), round(doc[IndexEnum.MAP] * 100, 3))

        self.l_cross[
            'text'] = 'op {} {}\nT {}\nkd {} {}\nmacd {} {}\n dmi {} {}\nrsi {} {}'.format(
            round(doc[TechEnum.KD_POINT], 3),
            round(doc[IndexEnum.PREDICT_CROSS_PRICE], 3),
            round((doc[IndexEnum.CLOSE] - doc[IndexEnum.DV30]) / 30 / doc[IndexEnum.MA30] * 100, 2),
            doc[TechEnum.KD_CROSS].value,
            doc[TechEnum.KD_TREND].value,
            doc[TechEnum.MACD_CROSS].value,
            doc[TechEnum.MACD_TREND].value,
            doc[TechEnum.DMI_CROSS].value,
            doc[TechEnum.DMI_TREND].value,
            doc[TechEnum.RSI_CROSS].value,
            doc[TechEnum.RSI_TREND].value)
        self.l_ma['text'] = 'm5 {} {}\nm1 {} {}\nm8 {} {}\nm2 {} {}\nm3 {} {}\nm5 {} {}'.format(
            round(doc[IndexEnum.MA5], 3),
            round(doc[IndexEnum.DV5], 3),
            round(doc[IndexEnum.MA10], 3),
            round(doc[IndexEnum.DV10], 3),
            round(doc[IndexEnum.MA18], 3),
            round(doc[IndexEnum.DV18], 3),
            round(doc[IndexEnum.MA20], 3),
            round(doc[IndexEnum.DV20], 3),
            round(doc[IndexEnum.MA30], 3),
            round(doc[IndexEnum.DV30], 3),
            round(doc[IndexEnum.MA50], 3),
            round(doc[IndexEnum.DV50], 3))
        self.l_kd['text'] = 'kd {} {}'.format(round(doc[IndexEnum.K], 3), round(doc[IndexEnum.D], 3))
        self.l_kd.configure(
            fg=LineColorEnum.SELL_WIN.value if doc[IndexEnum.K] > doc[IndexEnum.D] else LineColorEnum.SELL_LOSS.value)
        self.l_rsi['text'] = 'rsi5/10 {} {}'.format(round(doc[IndexEnum.RSI5], 3), round(doc[IndexEnum.RSI10], 3))
        self.l_rsi.configure(
            fg=LineColorEnum.SELL_WIN.value if doc[IndexEnum.RSI5] > doc[
                IndexEnum.RSI10] else LineColorEnum.SELL_LOSS.value)
        self.l_bband1['text'] = 'ub mb lb 1\n{} {} {}'.format(round(doc[IndexEnum.UPPERBAND1], 3),
                                                              round(doc[IndexEnum.MIDDLEBAND], 3),
                                                              round(doc[IndexEnum.LOWERBAND1], 3))
        self.l_bband2['text'] = 'ub lb 2\n{} {}'.format(round(doc[IndexEnum.UPPERBAND2], 3),
                                                        round(doc[IndexEnum.LOWERBAND2], 3))
        self.l_macd['text'] = 'dif mac his\n{} {} {} {}'.format(round(doc[IndexEnum.DIFF], 3),
                                                                round(doc[IndexEnum.MACD], 3),
                                                                round(doc[IndexEnum.HIST], 3),
                                                                round(doc[TechEnum.MACD_POINT], 3))
        self.l_macd.configure(
            fg=LineColorEnum.SELL_WIN.value if doc[IndexEnum.DIFF] > doc[
                IndexEnum.MACD] else LineColorEnum.SELL_LOSS.value)
        self.l_dmi['text'] = 'adx +di -di\n{} {} {}'.format(round(doc[IndexEnum.ADX], 3),
                                                            round(doc[IndexEnum.PLUS_DI], 3),
                                                            round(doc[IndexEnum.MINUS_DI], 3))
        self.l_dmi.configure(
            fg=LineColorEnum.SELL_WIN.value if doc[IndexEnum.PLUS_DI] > doc[
                IndexEnum.MINUS_DI] else LineColorEnum.SELL_LOSS.value)

        trade = ""
        if doc[IndexEnum.DATE] in self.buy_map:
            trade += "Buy %.3f\n" % self.buy_map[doc[IndexEnum.DATE]]
        if doc[IndexEnum.DATE] in self.sell_win_map:
            trade += "Win % .3f\n" % self.sell_win_map[doc[IndexEnum.DATE]]
        if doc[IndexEnum.DATE] in self.sell_loss_map:
            if doc[IndexEnum.DATE] in self.stop_map:
                trade += "Stop % .3f\n" % self.stop_map[doc[IndexEnum.DATE]]
            else:
                trade += "Loss % .3f\n" % self.sell_loss_map[doc[IndexEnum.DATE]]
        if doc[IndexEnum.DATE] in self.buy_fail_map:
            trade += "Buy Fail % .3f\n" % self.buy_fail_map[doc[IndexEnum.DATE]]
        if doc[IndexEnum.DATE] in self.sell_fail_map:
            trade += "Sell Fail % .3f\n" % self.sell_fail_map[doc[IndexEnum.DATE]]
        self.l_trade['text'] = trade
