class CommonTool:
    @staticmethod
    def price_rule(_price):
        if not _price:
            return
        if _price < 10:
            return round(_price, 2)
        elif _price < 50:
            return round(int(_price // 0.05) * 0.05 + (0.05 if _price % 0.05 > 0.025 else 0), 2)
        elif _price < 100:
            return round(_price)
        elif _price < 500:
            return int(_price // 0.5) * 0.5 + (0.5 if _price % 0.5 > 0.25 else 0)
        elif _price < 1000:
            return round(_price)
        else:
            return round(int(_price // 5) * 5 + (5 if _price % 5 > 2.5 else 0))

    @staticmethod
    def amount_to_string(value):
        """
            單位轉換，金額轉億元
        :param value:
        """
        return "%.2f " % (value / 100000000)
