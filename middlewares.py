import logging


class Middlewares:
    def check_input(self, page):
        if not page.ok:
            logging.error("target server connection error")
            return None
        content = self.page.json()
        if content['stat'] == 'OK':
            return content
        else:
            logging.error('can not get {} data at {}: {}'.format(self.table_name, self.date, content['stat']))
            return None

