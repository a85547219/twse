from functools import lru_cache

from datetime import timedelta
from ServiceImplement.ParserImpl.tpex import OTC, ItTrade
from apscheduler.schedulers.background import BackgroundScheduler
import time
from ServiceImplement.ParserImpl.financial import Mon, QUA
from ServiceImplement.DatabaseImpl.cyqRepository import CyqRepo
import collections
from ServiceImplement.AnalysisImpl.coinStrategy import CoinStrategy
from ServiceImplement.DatabaseImpl.tpexRepository import *
from ServiceImplement.DatabaseImpl.twtbRepository import Twtb4uRepo
from ServiceImplement.DatabaseImpl.miIndexRepository import MiIndexRepo
from ServiceImplement.DatabaseImpl.iiRepository import IiSumRepo, IiRepo
from ServiceImplement.DatabaseImpl.stockNoRepository import StockNoTable
from ServiceImplement.DatabaseImpl.finRepository import MonRepo, QuaRepo
from commonCode import CategoryEnum
from Service.databaseService import Database
from ServiceImplement.ParserImpl.exchange import MI
from datetime import datetime
from Service.parserService import Parser
import socket


class Singleton(object):
    _instance = None

    def __new__(cls, *args, **kwargs):
        if not isinstance(cls._instance, cls):
            cls._instance = object.__new__(cls, *args, **kwargs)

        return cls._instance


class A(Singleton):

    @property
    def db(self):
        return self._db

    def get_name(self):
        print(self.name)


class B(Singleton):
    def __init__(self):
        self.a = A()


class D(B):
    name = "D"


class C(B):
    name = "B"


def interval_trigger(job, args, second):
    sched = BackgroundScheduler()
    sched.add_job(job, 'interval', seconds=second, args=args)
    sched.start()


@lru_cache(maxsize=32)
def main_job():
    tmp = [
        "17897987979879879879879879879879879879879797897987979789797987987979798978978978978978978978978978978997879888888888888888888888797897987979981777777891761761769871698716987169871698716971" * 3000]
    time.sleep(3)
    return tmp


def isPalindrome(s: str) -> bool:
    s = s.lower()
    ss = ""
    for _s in s:
        if ord('z') >= ord(_s) >= ord('a') or 58 > ord(_s) > 47:
            ss += _s
    print(ss)
    tmp = 0
    n = len(ss)
    while tmp < n / 2:
        print(ss[tmp], ss[n - tmp - 1])
        if ss[tmp] != ss[n - tmp - 1]:
            return False
        tmp += 1
    return True


class Node(object):
    def __init__(self):
        self.children = collections.defaultdict(Node)
        # self.children = {}
        self.isword = False


class Tree:
    def __init__(self):
        self.db = {}
        self.end = False


class C:
    def C_math(self):
        self.current_max = 100
        self.col = []
        self.col.append((10, self.current_max))
        self.current_max = 20
        print(self.col)

    def res(self, cur, n, x):
        if n < x:
            return
        if x == 0 and n == 0:
            self.ans.append(cur)
            return
        if n == 0:
            return
        for i in range(2):
            self.res(cur + str(i), n - 1, x - i)

    def find_number_min(self, nums: list, n: int) -> list:
        self.ans = []
        return self.ans

    def findMaximumXOR(self, nums) -> int:
        answer = 0
        for i in range(4)[::-1]:
            answer <<= 1
            prefixes = {num >> i for num in nums}
            print(prefixes)
            answer += any(answer ^ 1 ^ p in prefixes for p in prefixes)
            print(answer)
        return answer


if __name__ == '__main__':
    a = CoinStrategy('1234', 5)
    b = CoinStrategy('4321', 5)
    a.from_days = 20
    print(b.from_days)
    # from_date = datetime.strptime('20200904', '%Y%m%d')
    # CyqRepo().remove_by_date(from_date)
    # print(isPalindrome("0P"))
    # from_date = datetime.strptime('20200727', '%Y%m%d')
    # Tpex3ItRepo().remove_by_date(from_date)
    # TpexIndex().remove_by_date(from_date)
    # to_date = datetime.now()
    #
    # while from_date <= to_date:
    #     if from_date.weekday() in [5, 6]:
    #         from_date += timedelta(1)
    #         continue
    #     print(from_date)
    #     ItTrade(from_date).run_spider()
    #     from_date += timedelta(1)

    # date = datetime.strptime('20140101', '%Y%m%d')

    # date = datetime.strptime('20200525', '%Y%m%d')
    # sn = '8016'
    # qua = QuaRepo().get_by_sn(sn)
    # mon = MonRepo().get_by_sn(sn)
    # mi = MiIndexRepo().get_month_by_sn(sn)
    # print(qua, mon, mi)

# print(CategoryEnum('').name)
# Parser().fin_mon_parser(datetime.now())

# ip_port = '192.168.1.24', 35003
#
# sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# sock.connect(ip_port)
# message = "TVMREC9064DR10.100.63.9    102093090006320200522080000110009309\n"
# # sock.sendto(message.encode(), ip_port)
# sock.send(message.encode())
# sock.send(message.encode())
# print(sock.recv(1024))

# date = datetime.strptime('20200525', '%Y%m%d')
# MI(date).run_spider()

#
# print("0")
# nameMap = StockNoTable().get_map()
#
# print("1")
# nameMap = StockNoTable().get_map()
# print("2")
# # repo_list = [IiSumRepo() for _ in range(2000)]
# # for x in repo_list:
# #     date = datetime.strptime('20200206', '%Y%m%d')
# #     cursor = x.get_sum_by_date(date)
# # #     print(cursor[0])
# # now_time = datetime.now().replace(day=21, hour=0, minute=0, second=0, microsecond=0)
# # rst = Database.get_summary_by_date(now_time)
# # print(rst)
# #
# # ii_sum = IiSumRepo()
# # mi_index = MiIndexRepo()
# # stockNoTable = StockNoTable()
#
# # cursor = Database.get_summary_by_date(date)
# # print(cursor[0])
# #
# # cursor = stockNoTable.get_map()
# # cursor = mi_index.get_lasted_data()
# # print(cursor)
# # b1 = C()
# # b2 = C()
# # start = datetime.now().timestamp()
# # Database.get_doc_by_name("2344")
# # end = datetime.now().timestamp()
# # print(end - start)
# # start = datetime.now().timestamp()
# # ii_data = Database.get_ii_by_name("2344")
# # end = datetime.now().timestamp()
# # print(end - start)
# # html = PrintReport.summary_to_html(cursor)
# # print(r"{}".format(html))
#
# twtMap = Twtb4uRepo().find_by_date(date)
# vol = MiIndexRepo().find_by_date(date)
#
# for twt in vol:
#     twt['N'] = nameMap.get(twt['SN'])['NAME']
#     twt['DT'] = twtMap.get(twt['SN'], 0)
#     twt['RATE'] = round(twt['DT'] / (1 if twt['V'] == 0 else twt['V']), 3)
#
# newList = sorted([v for v in vol if v['RATE'] > 0.3 and v['V'] > 2000], key=lambda k: k['RATE'])
# print(Twtb4uRepo)
