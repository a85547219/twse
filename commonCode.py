from enum import Enum


class TradeAction(Enum):
    BUY = '建議買進'
    SELL = '建議賣出'
    MUST_BUY = '強烈建議買進'
    MUST_SELL = '強烈建議賣出'
    DO_NOTHING = '無'
    DO_STOP = '停損'
    NOTICE_BUY = '關注買入'
    NOTICE_SELL = '關注賣出'


class CategoryEnum(Enum):
    C0 = ''
    C1 = '光電業'
    C2 = '其他業'
    C3 = '其他電子業'
    C4 = '化學工業'
    C5 = '半導體業'
    C6 = '塑膠工業'
    C7 = '建材營造業'
    C8 = '橡膠工業'
    C9 = '水泥工業'
    C10 = '汽車工業'
    C11 = '油電燃氣業'
    C12 = '玻璃陶瓷'
    C13 = '生技醫療業'
    C14 = '紡織纖維'
    C15 = '航運業'
    C16 = '觀光事業'
    C17 = '貿易百貨業'
    C18 = '資訊服務業'
    C19 = '通信網路業'
    C20 = '造紙工業'
    C21 = '金融保險業'
    C22 = '鋼鐵工業'
    C23 = '電器電纜'
    C24 = '電子通路業'
    C25 = '電子零組件業'
    C26 = '電機機械'
    C27 = '電腦及週邊設備業'
    C28 = '食品工業'


class TechEnum(Enum):
    MA18_TREND = 'MA18_TREND'
    MA18_CROSS = 'MA18_CROSS'
    MA18_POINT = 'MA18_POINT'
    MA10_TREND = 'MA13_TREND'
    MA10_CROSS = 'MA13_CROSS'
    MA10_POINT = 'MA13_POINT'
    KD_TREND = 'KD_TREND'
    KD_CROSS = 'KD_CROSS'
    KD_POINT = 'KD_POINT'
    BBAND_TREND = 'BBAND_TREND'
    BBAND_CROSS = 'BBAND_CROSS'
    BBAND_POINT = 'BBAND_POINT'
    DMI_TREND = 'DMI_TREND'
    DMI_CROSS = 'DMI_CROSS'
    DMI_POINT = 'DMI_POINT'
    MACD_TREND = 'MACD_TREND'
    MACD_CROSS = 'MACD_CROSS'
    MACD_POINT = 'MACD_POINT'
    RSI_TREND = 'RSI_TREND'
    RSI_CROSS = 'RSI_CROSS'
    RSI_POINT = 'RSI_POINT'
    TREND = '趨勢'


class IndexEnum(Enum):
    DATE = '日期'
    OPEN = '開盤'
    CLOSE = '收盤'
    HIGH = '最高'
    LOW = '最低'
    VOL = '量(張)'
    CI = '外陸資'
    FIDL = '外資自營商'
    IT = '投信'
    DL_SD = '自營商-自買'
    DL_HG = '自營商-避險'
    RT = '法人交易量'
    MAP = '最大震幅'
    AP = '震幅'
    BIAS = 'BIAS'
    MA5 = '5均線'
    DV5 = '5扣抵'
    MA10 = '10均線'
    DV10 = '10扣抵'
    MA18 = '18均線'
    DV18 = '18扣抵'
    MA20 = '20均線'
    DV20 = '20扣抵'
    MA30 = '30均線'
    DV30 = '30扣抵'
    MA50 = '50均線'
    DV50 = '50扣抵'
    RSI5 = 'RSI_5'
    RSI10 = 'RSI_10'
    MIDDLEBAND = 'MIDDLEBAND'
    UPPERBAND1 = 'UPPERBAND_1'
    LOWERBAND1 = 'LOWERBAND_1'
    UPPERBAND2 = 'UPPERBAND_2'
    LOWERBAND2 = 'LOWERBAND_2'
    K = 'K'
    D = 'D'
    PREDICT_CROSS_PRICE = '預估下次KD轉折價位'
    FAST_K = 'RSV'
    FAST_D = 'MA_RSV'
    DIFF = 'DIFF'
    MACD = 'MACD'
    HIST = 'HIST'
    ADX = 'ADX'
    PLUS_DI = '+DI'
    MINUS_DI = '-DI'
    VOL_OVERBOOKING = '今日量/五日均量'


class TrendEnum(Enum):
    SMOOTH_AFTER_UP = 'S_A_U'
    SMOOTH_AFTER_DOWN = 'S_A_D'
    UP_BURST = 'U_B'
    UP_NORMAL = 'U_N'
    UP_WEAK = 'U_W'
    DOWN_BURST = 'D_B'
    DOWN_NORMAL = 'D_N'
    DOWN_WEAK = 'D_W'


TrendEnum_UP_S = TrendEnum.UP_BURST, TrendEnum.UP_NORMAL
TrendEnum_DOWN_S = TrendEnum.DOWN_BURST, TrendEnum.DOWN_NORMAL
TrendEnum_SMOOTH = TrendEnum.SMOOTH_AFTER_UP, TrendEnum.SMOOTH_AFTER_DOWN
TrendEnum_UP = TrendEnum.UP_BURST, TrendEnum.UP_WEAK, TrendEnum.UP_NORMAL
TrendEnum_DOWN = TrendEnum.DOWN_BURST, TrendEnum.DOWN_WEAK, TrendEnum.DOWN_NORMAL


class CrossOverEnum(Enum):
    FAST_UP = 'F_UP'
    FAST_DOWN = 'F_D'
    FAST_NO_CHANGE = 'F_N_C'
    GOLD_CROSS = 'G'
    DEAD_CROSS = 'D'
    AFTER_G = 'A_G'
    AFTER_D = 'A_D'
    NO_CROSS = 'N'


GOLD_STATUS = CrossOverEnum.GOLD_CROSS, CrossOverEnum.AFTER_G
DEAD_STATUS = CrossOverEnum.DEAD_CROSS, CrossOverEnum.AFTER_D


class LineColorEnum(Enum):
    PLUS_FAST = 'mediumblue'
    MINUS_SLOW = 'chocolate'
    OTHER = 'darkorchid'
    BBAND1 = 'dimgrey'
    BBAND2 = 'dimgray'
    BLACK = 'black'

    TAG_LINE = 'gold'
    BUY_TAG = 'grey'
    SELL_LOSS = 'green'
    SELL_WIN = 'red'
    STOP_LOSE = 'blueviolet'

    FAIL_BUY = 'darkorchid'
    FAIL_SELL = 'black'


class ReportEnum(Enum):
    STOCK_NO = 'Stock'
    WIN_RATE = 'W(%)'
    RETURN_AVG = 'R(%)'
    RETURN_ACC = 'R ACC'
    RETURN_MAX = 'MAX(%)'
    RETURN_MIN = 'MIN(%)'
    RETURN_MAX_ONE_TRADE = 'MAX ONE TRADE(%)'
    RETURN_MIN_ONE_TRADE = 'MIN ONE TRADE(%)'
    TRADE_TIMES = 'Trade'
    BUY_TIMES = 'Buy'
    WIN_TIMES = 'Win'
    LOSE_TIMES = 'Lose'
    BUY_FAIL = 'Buy Fail'
    SELL_FAIL = 'Sell Fail'
    STOP = 'Stop'
    KEEP_DAY_AVG = 'Avg. Keep'
    KEEP_DAY_LONG = 'Long Keep'
    KEEP_DAY_SHORT = 'Short Keep'


class SummaryReportEnum(Enum):
    TOTAL = 'Total'
    TOTAL_WITHOUT_TRADE = 'No Trading Count'
    TOTAL_WITH_TRADE = 'Trading Count'
    NO_TRADE_COLLECTION = 'No Trade Collection'
    WIN_RATE = 'W(%)'
    WIN_RATE_MIN = 'MIN_W(%)'
    WIN_RATE_MAX = 'MAX_W(%)'

    RETURN_ACC = '累積報酬(%)'
    RETURN_ACC_AVG = '累積報酬平均(%)'
    RETURN_ACC_MAX = '累積報酬MAX (%)'
    RETURN_ACC_MIN = '累積報酬MIN (%)'

    RETURN_AVG = '平均報酬(%)'
    RETURN_AVG_ACC = '平均報酬累積 (%)'
    RETURN_AVG_MAX = '平均報酬MAX (%)'
    RETURN_AVG_MIN = '平均報酬MIN (%)'

    RETURN_MAX_ONE_TRADE_AVG = 'MAX Avg.R(%)'
    RETURN_MIN_ONE_TRADE_AVG = 'MIN Avg. R(%)'
    RETURN_MAX_ONE_TRADE = 'MAX ONE TRADE(%)'
    RETURN_MIN_ONE_TRADE = 'MIN ONE TRADE(%)'

    TRADE_TIMES = 'Trade Cnt.'
    TRADE_TIMES_AVG = 'Trade Avg.'
    BUY_TIMES = 'Buy'
    BUY_TIMES_AVG = 'Buy Avg.'
    WIN_TIMES = 'Win'
    WIN_TIMES_AVG = 'Win Avg.'
    LOSE_TIMES = 'Lose'
    LOSE_TIMES_AVG = 'Loss Avg.'
    BUY_FAIL_TIMES = 'Buy Fail'
    BUY_FAIL_TIMES_AVG = 'Buy Fail Avg.'
    SELL_FAIL_TIMES = 'Sell Fail'
    SELL_FAIL_TIMES_AVG = 'Sell Fail Avg.'
    STOP_TIMES = 'Stop'
    STOP_TIMES_MAX = 'Stop Max'
    STOP_TIMES_AVG = 'Stop Avg.'
    KEEP_DAY_AVG = 'Keep Avg.'
    KEEP_DAY_LONG = 'Long Keep'
    KEEP_DAY_LONG_AVG = 'Long Keep Avg.'
    KEEP_DAY_SHORT = 'Short Keep'
    KEEP_DAY_SHORT_AVG = 'Short Keep Avg.'


class AnalysisResultEnum(Enum):
    BASIC = 'Basic'
    DETAIL = 'Detail'
    FIGURE = 'Fig'
    REPORT = 'Report'
    ORDER = 'Order'
    OTHER = 'Other'


class RunModeEnum(Enum):
    ONE = 'One'
    MANY = 'Many'
    MAIL = 'Mail'


class RecordEnum(Enum):
    KD = '黃金/死亡交叉，日期，價位，KD交叉點'
